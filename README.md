# **/ [Projet Sprotif](https://gitlab.com/GGestin/goodenough-l3-ii)**
##### Projet de Conception d'Application
##### Licence 3 Ingénierie Informatique
| Contributeurs |
|--|
| Fifre Hugo |
| Gestin Gaëtan |
| Jézéquel Clément |
| Ramos Giquel Joshua |

# Sommaire
### 1. [How2Git](https://gitlab.com/GGestin/goodenough-l3-ii)
### 2. [Dates clé](https://gitlab.com/GGestin/goodenough-l3-ii)
### 3. [Documentation](https://gitlab.com/GGestin/goodenough-l3-ii)
### 4. [Rendu](https://gitlab.com/GGestin/goodenough-l3-ii)
<hr>

## 1. How2Git
*Pour eviter les demandes de login tout le temps :*

`git config --global user.name "XXX"`
`git config --global user.email "xxx@gmail.com"`

*Telecharger le git :*

`git clone https://gitlab.com/GGestin/goodenough-l3-ii`

*Méthode pour push :*
1. Ajouter les modifs d'abord (faire à la racine du git (goodenough-l3-ii)):

`git add .`

1. On peut check le status pour voir ce que l'on modifie, ajoute, supprime

`git status`

1. Pis on commit (push local) :

`git commit -m "Message simple mais précis"`
1. On pull les modifs avant de push :

`git pull`

1. Puis on push enfin

`git push`

## 2. Dates clé

| Date | Sprint + Titre |
|--|--|
| 22/01/2021 | **Sprint 1** : Faire les classes et les programmes de tests pour la gestion des données |
| 12/02/2021 | **Sprint 2** : Faire l’application Web de suivi et la base de données |
| 25/02/2021 | **Sprint 3** : Mettre en place des évolutions |

## 3. Documentation

Lien du [Drive](https://drive.google.com/drive/folders/1lI1fSpH-Y04McFeZFV2v-lKercElU5D5?usp=sharing)

1. [TODO]()
1. [TODO]()
1. [TODO]()

## 4. Rendu
1. [TODO]()
1. [TODO]()
1. [TODO]()
