package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import application.questionnaire.ListeQuestionnaires;
import application.questionnaire.Questionnaire;
import java.util.ArrayList;
import org.junit.Test;


public class TestListeQuestionnaire {

  @Test
  public void testConstructeurListeQuestionnaire() {
    // init
    Questionnaire questionnaire1 = new Questionnaire("Un questionnaire", true, 1);
    Questionnaire questionnaire2 = new Questionnaire("Un questionnaire", true, 2);

    // constructeur null
    ListeQuestionnaires listeQuestionnaires1 = new ListeQuestionnaires(null);
    assertNull(listeQuestionnaires1.getListeQuestionnaires());

    // constructeur normal
    ArrayList<Questionnaire> listeQ = new ArrayList<Questionnaire>();
    listeQ.add(questionnaire1);
    listeQ.add(questionnaire2);

    ListeQuestionnaires listQuestionnaires2 = new ListeQuestionnaires(listeQ);
    assertEquals("La taille de la liste doit etre de : 2", 2,
        listQuestionnaires2.getListeQuestionnaires().size());

  }

  @Test
  public void testAjoutQuestionnaire() {
    // init
    ArrayList<Questionnaire> listeQ = new ArrayList<Questionnaire>();
    ListeQuestionnaires listeQuestionnaires = new ListeQuestionnaires(listeQ);

    // constructeur null
    listeQuestionnaires.ajoutQuestionnaire(0, null, false);
    assertEquals("La taille de la liste doit etre de : 0", 0,
        listeQuestionnaires.getListeQuestionnaires().size());

    // constructeur normal
    listeQuestionnaires.ajoutQuestionnaire(3, "Un questionnaire", true);

    assertEquals("La taille de la liste doit etre de : 1", 1,
        listeQuestionnaires.getListeQuestionnaires().size());

  }

  @Test
  public void testSetListeQuestions() {
    // init
    ArrayList<Questionnaire> listeQ = new ArrayList<Questionnaire>();
    ListeQuestionnaires listeQuestionnaires = new ListeQuestionnaires(listeQ);

    // constructeur null
    listeQuestionnaires.setListeQuestionnaires(null);
    assertNull(listeQuestionnaires.getListeQuestionnaires());

    // constructeur normal
    listeQuestionnaires.setListeQuestionnaires(listeQ);

    assertEquals("La taille de la liste doit etre de : 0", 0,
        listeQuestionnaires.getListeQuestionnaires().size());
  }

}
