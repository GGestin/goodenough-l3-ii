package tests;

import static org.junit.Assert.assertEquals;

import application.questionnaire.Question;
import application.questionnaire.Questionnaire;
import java.util.ArrayList;
import org.junit.Test;


public class TestQuestionnaire {

  @Test
  public void testConstructeurQuestionnaire() {
    // initialisation d'une liste vide

    Questionnaire testQuestionnaire1 = new Questionnaire("Test1", false, 1);

    // premier test
    assertEquals("titre doit etre egale a : Test1", "Test1", testQuestionnaire1.getTitre());
    assertEquals("Ouverture doit etre egale a : false", false, testQuestionnaire1.isEstOuvert());

    // cr�er un questionnaire mais avec une ouverture � true
    Questionnaire testQuestionnaire2 = new Questionnaire("Test2", true, 2);
    assertEquals("L ouverture doit etre egale a : true", true, testQuestionnaire2.isEstOuvert());
    assertEquals("la taille doit etre egale a : 0", 0,
        testQuestionnaire2.getListeQuestions().size());

    // Cr�ation d'une question � ajouter dans la liste
    Questionnaire testQuestionnaire4 = new Questionnaire("Test4", true, 4);
    testQuestionnaire4.ajoutQuestion(0, "une question ?", 0);
    assertEquals("la taille doit etre egale a : 1", 1,
        testQuestionnaire4.getListeQuestions().size());
    // System.out.println(testQuestionnaire4.getListeQuestions().get(0)); MARCHE PAS
  }

  @Test
  public void testAjoutQuestionnaire() {
    // initialisation
    Questionnaire questionnaire = new Questionnaire("Test1", false, 1);

    // Ajout d'un object null
    questionnaire.ajoutQuestion(0, null, null);
    assertEquals("la taille du questionnaire doit etre egale a : 0", 0,
        questionnaire.getListeQuestions().size());

    // Ajout d'une question normal, ouverte
    questionnaire.ajoutQuestion(0, "une question ?", 0);
    assertEquals("la taille du questionnaire doit etre egale à : 1", 1,
        questionnaire.getListeQuestions().size());

    // Ajout d'une deuxième question, fermée
    questionnaire.ajoutQuestion(0, "Es-ce une question ?", 0);
    assertEquals("la taille du questionnaire doit etre egale à : 2", 2,
        questionnaire.getListeQuestions().size());

    /*
     * for (Question question : questionnaire.getListeQuestions()) { System.out.println(question); }
     * MARCHE PAS
     */

    // Supression d'une question
    questionnaire.getListeQuestions().remove(1);
    assertEquals("la taille du questionnaire doit etre egale à : 1", 1,
        questionnaire.getListeQuestions().size());

    /*
     * for (Question question : questionnaire.getListeQuestions()) { System.out.println(question);
     * }MARCHE PAS
     */
  }

  @Test
  public void testSetListeQuestionnaires() {
    // initialisation
    Questionnaire questionnaire = new Questionnaire("Test1", false, 1);

    // Creation d'une nouvelle liste de question
    ArrayList<Question> newListeDeQuestion = new ArrayList<Question>();
    newListeDeQuestion.add(new Question(0, "Je mange quoi aujourd'hui ?", 0));

    questionnaire.setListeQuestions(newListeDeQuestion);

    assertEquals("la taille du questionnaire doit etre egale à : 1", 1,
        questionnaire.getListeQuestions().size());

    // set null
    questionnaire.setListeQuestions(null);

    assertEquals("la taille du questionnaire doit etre egale à : 1", 1,
        questionnaire.getListeQuestions().size());
  }

  @Test
  public void testSetEstOuvert() {
    // initialisation
    Questionnaire questionnaire = new Questionnaire("Test1", false, 1);

    // set false alors qu'il est déjà en false
    questionnaire.setEstOuvert(false);

    assertEquals("L'ouverture doit etre : false", false, questionnaire.isEstOuvert());

    // set true alors qu'il est en false
    questionnaire.setEstOuvert(true);

    assertEquals("L'ouverture doit etre : true", true, questionnaire.isEstOuvert());
  }

}

