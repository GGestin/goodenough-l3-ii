package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import application.sportif.SportType;
import application.sportif.Sportif;
import java.time.LocalDate;
import org.junit.Test;



public class TestSportif {

  @Test
  public void testConstructeurSportif() {
    // Test Classic d'un sportif
    try {

      Sportif sportif1 = new Sportif(0, "", "", "", "", null, null);

      // Test nom,prenom,pseudo,mdp vide

      assertNull(sportif1.getNom());
      assertNull(sportif1.getPrenom());
      assertNull(sportif1.getPseudo());
      assertNull(sportif1.getMotDePasse());


      // Test nom,prenom,pseudo,mdp ne respecte pas les contraintes
      Sportif sportif2 = new Sportif(1, "a", "b", "aaaaaaaaaaaaaaaa", "abc", null, null);

      assertNull(sportif2.getNom());
      assertNull(sportif2.getPrenom());
      assertNull(sportif2.getPseudo());
      assertNull(sportif2.getMotDePasse());

      // creation normal d un sportif
      Sportif sportif3 = new Sportif(2, "Good", "Enough", "JDG", "!aA1bB2!",
          LocalDate.of(1987, 3, 11), SportType.BADMINTON);

      assertEquals("Nom du sportif doit etre : Good", "Good", sportif3.getNom());
      assertEquals("Prenom du sportif doit etre : Enough", "Enough", sportif3.getPrenom());
      assertEquals("Pseudo du sportif doit etre : JDG", "JDG", sportif3.getPseudo());
      assertEquals("MotDePasse du sportif doit etre : !aA1bB2!", "!aA1bB2!",
          sportif3.getMotDePasse());

      // Test nom,prenom avec un chiffre
      Sportif sportif4 = new Sportif(3, "Marche23", "Pas69", "", "", null, null);

      assertNull(sportif4.getNom());
      assertNull(sportif4.getPrenom());
      assertNull(sportif4.getPseudo());
      assertNull(sportif4.getMotDePasse());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testSetNom() {
    // init
    Sportif sportif = null;
    try {
      sportif = new Sportif(4, "Good", "Enough", "JDG", "!aA1bB2!", LocalDate.of(1987, 3, 11),
          SportType.BADMINTON);
    } catch (Exception e) {
      e.printStackTrace();
    }

    // taille de nom <= 1
    sportif.setNom("a");
    assertEquals("Nom du sportif doit etre : Good", "Good", sportif.getNom());

    // nom avecc un chiffre
    sportif.setNom("Josh13");
    assertEquals("Nom du sportif doit etre : Good", "Good", sportif.getNom());

    // test fonctionnel
    sportif.setNom("notGood");
    assertEquals("Nom du sportif doit etre : notGood", "notGood", sportif.getNom());

    // test vide
    sportif.setNom("");
    assertEquals("Nom du sportif doit etre : notGood", "notGood", sportif.getNom());

    // test null
    sportif.setNom(null);
    assertEquals("Nom du sportif doit etre : notGood", "notGood", sportif.getNom());
  }

  @Test
  public void testSetPrenom() {
    // init
    Sportif sportif = null;
    try {
      sportif = new Sportif(5, "notGood", "Enough", "JDG", "!aA1bB2!", LocalDate.of(1987, 3, 11),
          SportType.BADMINTON);
    } catch (Exception e) {
      e.printStackTrace();
    }

    // taille de nom <= 1
    sportif.setPrenom("b");
    assertEquals("Prenom du sportif doit etre : Enough", "Enough", sportif.getPrenom());

    // prenom avecc un chiffre
    sportif.setPrenom("killian8");
    assertEquals("Prenom du sportif doit etre : Enough", "Enough", sportif.getPrenom());

    // test fonctionnel
    sportif.setPrenom("notEnough");
    assertEquals("Prenom du sportif doit etre : notEnough", "notEnough", sportif.getPrenom());

    // test vide
    sportif.setPrenom("");
    assertEquals("Prenom du sportif doit etre : notEnough", "notEnough", sportif.getPrenom());

    // test null
    sportif.setPrenom(null);
    assertEquals("Prenom du sportif doit etre : notEnough", "notEnough", sportif.getPrenom());

  }

  @Test
  public void testSetPseudo() {
    // init
    Sportif sportif = null;
    try {
      sportif = new Sportif(6, "notGood", "notEnough", "JDG", "!aA1bB2!", LocalDate.of(1987, 3, 11),
          SportType.BADMINTON);
    } catch (Exception e) {
      e.printStackTrace();
    }

    // test fonctionnel
    sportif.setPseudo("notJDG");
    assertEquals("Pseudo du sportif doit etre : notJDG", "notJDG", sportif.getPseudo());

    // test vide
    sportif.setPseudo("");
    assertEquals("Pseudo du sportif doit etre : notJDG", "notJDG", sportif.getPseudo());

    // test au dessus de 15 caractere
    sportif.setPseudo("nootnootnootnootnootJDG");
    assertEquals("Pseudo du sportif doit etre : notJDG", "notJDG", sportif.getPseudo());

    // test null
    sportif.setPseudo(null);
    assertEquals("Pseudo du sportif doit etre : notJDG", "notJDG", sportif.getPseudo());

  }

  @Test
  public void testSetMotDePasse() {
    // init
    try {
      Sportif sportif = new Sportif(7, "notGood", "notEnough", "notJDG", "!aA1bB2!",
          LocalDate.of(1987, 3, 11), SportType.BADMINTON);

      sportif.setMotDePasse(null);
      assertEquals("Le mot de passe du sportif doit etre : !aA1bB2!", "!aA1bB2!",
          sportif.getMotDePasse());

      // taille du mot de passe inferieur a 8
      sportif.setMotDePasse("!aA1");
      assertEquals("Le mot de passe du sportif doit etre : !aA1bB2!", "!aA1bB2!",
          sportif.getMotDePasse());

      // taille du mot de passe superieur a 20
      sportif.setMotDePasse("!aA1a1z3eae65zrAZERRG654151SDZE");
      assertEquals("Le mot de passe du sportif doit etre : !aA1bB2!", "!aA1bB2!",
          sportif.getMotDePasse());

      // test fonctionnel
      sportif.setMotDePasse("(aaAA12)");
      assertEquals("Le mot de passe du sportif doit etre : (aaAA12)", "(aaAA12)",
          sportif.getMotDePasse());

      // test caractere special manquant
      sportif.setMotDePasse("aacbbAA1");
      assertEquals("Le mot de passe du sportif doit etre : (aaAA12)", "(aaAA12)",
          sportif.getMotDePasse());

      // test majuscule manquante
      sportif.setMotDePasse("aa!22bbac");
      assertEquals("Le mot de passe du sportif doit etre : (aaAA12)", "(aaAA12)",
          sportif.getMotDePasse());

      // test minusucle manquante
      sportif.setMotDePasse("AABB22!!!");
      assertEquals("Le mot de passe du sportif doit etre : (aaAA12)", "(aaAA12)",
          sportif.getMotDePasse());

      // test chiffre manquant
      sportif.setMotDePasse("aaAA!!bbB");
      assertEquals("Le mot de passe du sportif doit etre : (aaAA12)", "(aaAA12)",
          sportif.getMotDePasse());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testSetDateDeNaissance() {
    // init
    Sportif sportif = null;
    try {
      sportif = new Sportif(8, "notGood", "Enough", "JDG", "!aA1bB2!", LocalDate.of(1987, 3, 11),
          SportType.BADMINTON);
    } catch (Exception e) {
      e.printStackTrace();
    }

    // test fonctionnel
    sportif.setDateDeNaissance(LocalDate.of(2000, 6, 9));
    assertEquals("La date de naissance du sportif doit etre : Enough", LocalDate.of(2000, 6, 9),
        sportif.getDateDeNaissance());

    // test date de naissance superieur a la date d aujourd hui
    sportif.setDateDeNaissance(LocalDate.of(2077, 4, 20));
    assertEquals("La date de naissance du sportif doit etre : Enough", LocalDate.of(2000, 6, 9),
        sportif.getDateDeNaissance());

    // test null
    sportif.setDateDeNaissance(null);
    assertEquals("La date de naissance du sportif doit etre : Enough", LocalDate.of(2000, 6, 9),
        sportif.getDateDeNaissance());

  }

  @Test
  public void testSetActiviteSportive() {
    // init
    Sportif sportif = null;
    try {
      sportif = new Sportif(9, "notGood", "Enough", "JDG", "!aA1bB2!", LocalDate.of(1987, 3, 11),
          SportType.BADMINTON);
    } catch (Exception e) {
      e.printStackTrace();
    }

    // test fonctionnel
    sportif.setActiviteSportive(SportType.SKATEBOARD);
    assertEquals("L activite sportive du sportif doit etre : SportType.SKATEBOARD",
        SportType.SKATEBOARD, sportif.getActiviteSportive());

    // test null
    sportif.setActiviteSportive(null);
    assertEquals("L activite sportive du sportif doit etre : SportType.SKATEBOARD",
        SportType.SKATEBOARD, sportif.getActiviteSportive());

  }
}
