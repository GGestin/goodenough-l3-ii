package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import application.questionnaire.Questionnaire;
import application.questionnaire.Reponses;
import application.sportif.SportType;
import application.sportif.Sportif;
import java.time.LocalDate;
import java.util.ArrayList;
import org.junit.Test;

public class TestReponses {

  @Test
  public void testConstructeurReponses() {
    // init
    ArrayList<Integer> listeReponses = new ArrayList<Integer>();
    listeReponses.add(1);
    listeReponses.add(0);
    Sportif sportif = null;
    try {
      sportif = new Sportif(1, "notGood", "notEnough", "notJDG", "!aA1bB2!",
          LocalDate.of(1987, 3, 11), SportType.BADMINTON);
    } catch (Exception e) {
      e.printStackTrace();
    }
    Questionnaire questionnaire = new Questionnaire("Test1", false, 1);
    Reponses reponses1 =
        new Reponses(listeReponses, sportif, questionnaire, LocalDate.of(1987, 3, 11));

    assertEquals("la taille de la liste de reponse doit etre : 2", 2,
        reponses1.getListeReponses().size());

    assertEquals("Le nom du sportif doit etre : notGood", "notGood",
        reponses1.getSportifReponses().getNom());
    assertEquals("Le prenom du sportif doit etre : notEnough", "notEnough",
        reponses1.getSportifReponses().getPrenom());
    assertEquals("Le pseudo du sportif doit etre : notJDG", "notJDG",
        reponses1.getSportifReponses().getPseudo());

    assertEquals("Le titre du questionnaire doit etre : Test1", "Test1",
        reponses1.getQuestionnaireReponses().getTitre());
    assertEquals("L ouverture du questionnaire doit etre : false", false,
        reponses1.getQuestionnaireReponses().isEstOuvert());

    assertEquals("La date doit etre celle d aujourd hui", LocalDate.of(1987, 3, 11),
        reponses1.getDateReponses());

    // test avec des valeurs null
    Reponses reponses2 = new Reponses(null, null, null, null);
    assertEquals("la taille de la liste de reponse doit etre : 0", 0,
        reponses2.getListeReponses().size());
    assertNull(reponses2.getSportifReponses());
    assertNull(reponses2.getQuestionnaireReponses());
    assertEquals("La date doit etre celle d aujourd hui", LocalDate.now(),
        reponses2.getDateReponses());

  }

  @Test
  public void testSetQuestion() {
    // init
    ArrayList<Integer> listeReponses = new ArrayList<Integer>();
    listeReponses.add(0);
    listeReponses.add(1);
    Sportif sportif = null;
    try {
      sportif = new Sportif(1, "notGood", "notEnough", "notJDG", "!aA1bB2!",
          LocalDate.of(1987, 3, 11), SportType.BADMINTON);
    } catch (Exception e) {
      e.printStackTrace();
    }

    Questionnaire questionnaire = new Questionnaire("Test1", false, 1);
    Reponses reponses =
        new Reponses(listeReponses, sportif, questionnaire, LocalDate.of(1987, 3, 11));

    // ajout
    reponses.ajoutReponse(0);
    assertEquals("la taille de la liste de reponse doit etre : 3", 3,
        reponses.getListeReponses().size());

    // modif
    reponses.modifieReponse(0, 1);
    assertEquals("la taille de la liste de reponse doit etre : 1", new Integer(1),
        reponses.getListeReponses().get(0));

    // supression
    reponses.supprimeReponse(2);
    assertNull(reponses.getListeReponses().get(2));
  }

}
