package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import application.questionnaire.Question;
import org.junit.Test;


public class TestQuestion {

  @Test
  public void testConstructorQuestion() {
    // test contructeur vide
    Question question1 = new Question(0, "", 0);
    assertNull(question1.getTexteQuestion());

    // test normal
    Question question2 = new Question(1, "Une question ?", 1);
    assertEquals("Le text de la question doit etre : Une question ?", "Une question ?",
        question2.getTexteQuestion());
    assertEquals("La reponse de defaut de la question doit etre : vrai", true,
        question2.getReponseDefaut());
  }

  @Test
  public void testSetReponse() {
    // init
    Question question = new Question(1, "Une question ?", 1);
    // test true
    question.setReponseDefaut(1);
    assertEquals("La reponse de defaut de la question doit etre : vrai", true,
        question.getReponseDefaut());

    // test false
    question.setReponseDefaut(0);
    assertEquals("La reponse de defaut de la question doit etre : faux", false,
        question.getReponseDefaut());

  }

}
