
package application.questionnaire;


import java.util.ArrayList;

public class ListeQuestionnaires {

  ArrayList<Questionnaire> listeQuestionnaires;

  /**
   * Constructeur de la classe Questionnaire prenant tous les attributs.
   *
   * @param listeQuestionnaires Liste des questionnaires
   */
  public ListeQuestionnaires(ArrayList<Questionnaire> listeQuestionnaires) {
    super();
    if (listeQuestionnaires != null) {
      this.listeQuestionnaires = listeQuestionnaires;
    }
  }

  /**
   * Ajout d'un questionnaire dans la liste.
   *
   * @param titreQuestionnaire titre du questionnaire (non null et pas vide).
   * @param estOuvert dit si le questionnaire est ouvert ou non.
   * @return Vrai si il a bien �t� ajout�
   */
  public boolean ajoutQuestionnaire(int id, String titreQuestionnaire, boolean estOuvert) {
    if (titreQuestionnaire == null || titreQuestionnaire == "") {
      return false;
    }
    Questionnaire questionnaire = new Questionnaire(titreQuestionnaire, estOuvert, id);
    for (Questionnaire qs : listeQuestionnaires) {
      if (qs.getTitre().equals(questionnaire.getTitre())) {
        return false;
      }
    }
    return this.listeQuestionnaires.add(questionnaire);
  }

  // SETTERS AND GETTERS
  public ArrayList<Questionnaire> getListeQuestionnaires() {
    return this.listeQuestionnaires;
  }

  public void setListeQuestionnaires(ArrayList<Questionnaire> listeQuestionnaires) {
    this.listeQuestionnaires = listeQuestionnaires;
  }
}
