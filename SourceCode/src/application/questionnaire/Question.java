package application.questionnaire;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Question {

  int id;
  StringProperty texteQuestion;
  Integer reponseDefaut;

  /**
   * Une Question contenant un texte et une reponse par defaut (faux si null).
   *
   * @param i Indice de la Question.
   * @param texteQuestion Texte de la Question (non null et non vide).
   * @param reponseDefaut Reponse par defaut de la Question (False si null).
   */
  public Question(int i, String texteQuestion, Integer reponseDefaut) {
    super();

    if (texteQuestion != null && texteQuestion != "") {
      this.texteQuestion = new SimpleStringProperty(texteQuestion);
    }

    if (reponseDefaut == null || reponseDefaut.intValue() > 1 || reponseDefaut.intValue() < 0) {
      this.reponseDefaut = 1;
    } else {
      this.reponseDefaut = reponseDefaut;
    }

    this.id = i;
  }

  // SETTERS AND GETTERS

  public StringProperty getTexteProperty() {
    return texteQuestion;
  }

  /**
   * retourne le String de la variable texteQuestion.
   * @return Si l'attribut n est pas null alors il renvoie le string du texte de la question.
   */
  public String getTexteQuestion() {
    if (this.texteQuestion != null) {
      return texteQuestion.get();
    }
    return null;
  }

  /**
   * Modifie le texte de la question si le texte n'est pas vide ou null.
   *
   * @param texteQuestion le nouveau texte a appliquer.
   * @return vrai si la modification a bien eu lieu
   */
  public boolean setTexteQuestion(String texteQuestion) {
    if (texteQuestion != null && texteQuestion != "") {
      this.texteQuestion = new SimpleStringProperty(texteQuestion);
      return true;
    }
    return false;
  }

  public Integer getReponseDefaut() {
    return reponseDefaut;
  }

  /**
   * Modifie la Reponse par defaut de la question si la Reponse n'est pas null.
   *
   * @param reponseDefaut la nouvelle reponse par defaut (non null)
   */
  public void setReponseDefaut(Integer reponseDefaut) {
    if (reponseDefaut != null) {
      this.reponseDefaut = reponseDefaut;
    }
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }


  public String toString() {
    String ret = this.texteQuestion.get();
    return ret;
  }
}
