package application.questionnaire;

import application.sportif.Sportif;
import java.time.LocalDate;
import java.util.ArrayList;


public class ListeReponses {

  private ArrayList<Reponses> listeReponses;

  /**
   * Classe contenant la liste des r�ponses.
   *
   * @param listeReponses La liste des r�ponses des questionnaires (non null)
   */
  public ListeReponses(ArrayList<Reponses> listeReponses) {
    super();
    if (listeReponses != null) {
      this.listeReponses = listeReponses;
    }
  }

  /**
   * Ajout d'une Reponse d'un questionnaire dans la liste de Reponses.
   *
   * @param listeReponses liste des reponses possible des reponses (non null sinon vide).
   * @param sportifReponses le sportif qui a repondu.
   * @param questionnaireReponses le questionnaire qui a �t� r�pondu.
   * @param dateReponses la date a laquelle le questionnaire a �t� r�pondu.
   * @return vrai s'il a bien �t� ajout�.
   */
  public boolean ajoutReponses(ArrayList<Integer> listeReponses, Sportif sportifReponses,
      Questionnaire questionnaireReponses, LocalDate dateReponses) {
    Reponses reponses =
        new Reponses(listeReponses, sportifReponses, questionnaireReponses, dateReponses);
    return this.listeReponses.add(reponses);
  }

  // SETTERS AND GETTERS
  public ArrayList<Reponses> getListeReponses() {
    return this.listeReponses;
  }

  public void setListeReponses(ArrayList<Reponses> listeReponses) {
    this.listeReponses = listeReponses;
  }
}
