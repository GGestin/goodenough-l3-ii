package application.questionnaire;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Questionnaire {

  StringProperty titreQuestionnaire;
  ArrayList<Question> listeQuestions;
  int id;
  boolean estOuvert;

  /**
   * Constructeur de la classe Questionnaire prenant tous les attributs.
   *
   * @param titreQuestionnaire Titre du Questionnaire (non null et pas vide).
   * @param estOuvert Etat du questionnaire a la creation.
   */
  public Questionnaire(String titreQuestionnaire, boolean estOuvert, int id) {
    super();

    if (titreQuestionnaire != null && titreQuestionnaire != "") {
      this.titreQuestionnaire = new SimpleStringProperty(titreQuestionnaire);
    }

    this.listeQuestions = new ArrayList<Question>();

    this.estOuvert = estOuvert;

    this.id = id;
  }

  /**
   * Ajout d'une Question a un Questionnaire si elle n'a pas le m�me texte qu'une autre question.
   *
   * @param idQuestion l'indice de la question.
   * @param texteQuestion le text de la question (non null et pas vide et unique).
   * @param reponseDefaut le chiffre designant la reponse par defaut de la question (non null, pas
   *        superieur a 1 ou inferieur a 0).
   * @return vrai s'il a bien ajout�e une question dans la liste.
   */
  public boolean ajoutQuestion(int idQuestion, String texteQuestion, Integer reponseDefaut) {
    if (texteQuestion == null || texteQuestion == "" || reponseDefaut == null
        || reponseDefaut.intValue() > 1 || reponseDefaut.intValue() < 0) {
      return false;
    }
    Question question = new Question(idQuestion, texteQuestion, reponseDefaut);
    for (Question q : this.listeQuestions) {
      if (q.getTexteQuestion().equals(question.getTexteQuestion())) {
        return false;
      }
    }
    return this.listeQuestions.add(question);
  }

  /**
   * modifie le texte d'une question a l'index "index" de la liste. verification que le nouveau
   * texte ne contient pas le m�me texte qu'une autre question de la liste (unique).
   *
   * @param index la position de la question dans la liste (compris entre la taille maximum et
   *        minimum de la liste)
   * @param texteQuestion nouveau texte que l'on veut impl�menter (non null, non vide et unique)
   * @return vrai si la modification a bien eu lieu.
   */
  public boolean modifierTexteQuestion(int index, String texteQuestion) {
    if (texteQuestion == null || texteQuestion == "" || index > this.listeQuestions.size() - 1
        || index < 0) {
      return false;
    }
    for (Question question : this.listeQuestions) {
      if (texteQuestion.equals(question.getTexteQuestion())) {
        return false;
      }
    }
    return this.listeQuestions.get(index).setTexteQuestion(texteQuestion);
  }

  // SETTERS AND GETTERS
  public ArrayList<Question> getListeQuestions() {
    return listeQuestions;
  }

  /**
   * change la liste de question avec celle en parametre non null.
   *
   * @param listeQuestions liste non null de questions
   */
  public void setListeQuestions(ArrayList<Question> listeQuestions) {
    if (listeQuestions != null) {
      this.listeQuestions = listeQuestions;
    }
  }

  public boolean isEstOuvert() {
    return estOuvert;
  }

  public void setEstOuvert(boolean estOuvert) {
    this.estOuvert = estOuvert;
  }

  public StringProperty getTitreProperty() {
    return titreQuestionnaire;
  }

  public String getTitre() {
    return titreQuestionnaire.get();
  }

  public void setTitreQuestionnaire(String titre) {
    this.titreQuestionnaire.set(titre);
  }

  public void setTitreQuestionnaireProperty(StringProperty titre) {
    this.titreQuestionnaire = titre;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String toString() {
    String res = this.titreQuestionnaire.get();
    return res;
  }
}
