package application.questionnaire;

import application.sportif.Sportif;
import java.time.LocalDate;
import java.util.ArrayList;


public class Reponses {

  ArrayList<Integer> listeReponsesPossible;
  Sportif sportifReponses;
  Questionnaire questionnaireReponses;
  LocalDate dateReponses;

  /**
   * Classe contenant les r�ponses sous forme de liste d'entiers.
   *
   * @param listeReponses La liste des r�ponses.
   * @param sportifReponses Le sportif qui a r�pondu.
   * @param questionnaireReponses Questionnaire des Reponses.
   * @param dateReponses Date du rendu des responses.
   */
  public Reponses(ArrayList<Integer> listeReponses, Sportif sportifReponses,
      Questionnaire questionnaireReponses, LocalDate dateReponses) {
    super();
    if (listeReponses == null) {
      listeReponses = new ArrayList<Integer>();
    }
    if (dateReponses == null) {
      dateReponses = LocalDate.now();
    }

    this.listeReponsesPossible = listeReponses;
    this.sportifReponses = sportifReponses;
    this.questionnaireReponses = questionnaireReponses;
    this.dateReponses = dateReponses;
  }

  /**
   * Ajoute une reponse dans la liste de reponse,� une question au mme index que la question.
   *
   * @param reponse La reponse � ajouter
   */
  public void ajoutReponse(int reponse) {
    this.listeReponsesPossible.add(reponse);
  }

  /**
   * Modifie une reponse a une question au m�me index que la question.
   *
   * @param indexQuestion index de la Question (compris entre 0 et la taille max de la liste)
   * @param reponse La reponse � modifier
   */
  public void modifieReponse(int indexQuestion, int reponse) {
    if (indexQuestion < this.questionnaireReponses.getListeQuestions().size()
        && indexQuestion >= 0) {
      this.listeReponsesPossible.set(indexQuestion, reponse);
    }
  }

  /**
   * Supprime (rends null) une reponse a une question au m�me index que la question.
   *
   * @param indexQuestion index de la Question (compris entre 0 et la taille max de la liste)
   */
  public void supprimeReponse(int indexQuestion) {
    if (indexQuestion < this.questionnaireReponses.getListeQuestions().size()
        && indexQuestion >= 0) {
      this.listeReponsesPossible.set(indexQuestion, null);
    }
  }

  // SETTERS AND GETTERS
  public ArrayList<Integer> getListeReponses() {
    return this.listeReponsesPossible;
  }

  public void setListeReponses(ArrayList<Integer> listeReponses) {
    this.listeReponsesPossible = listeReponses;
  }

  public Sportif getSportifReponses() {
    return this.sportifReponses;
  }

  public void setSportifReponses(Sportif sportifReponses) {
    this.sportifReponses = sportifReponses;
  }

  public Questionnaire getQuestionnaireReponses() {
    return this.questionnaireReponses;
  }

  public void setQuestionnaireReponses(Questionnaire questionnaireReponses) {
    this.questionnaireReponses = questionnaireReponses;
  }

  public LocalDate getDateReponses() {
    return this.dateReponses;
  }

  public void setDateReponses(LocalDate dateReponses) {
    this.dateReponses = dateReponses;
  }
}
