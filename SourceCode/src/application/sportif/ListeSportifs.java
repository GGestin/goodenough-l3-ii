package application.sportif;

import java.time.LocalDate;
import java.util.ArrayList;

public class ListeSportifs {
  private ArrayList<Sportif> listeSportifs;

  /**
   * cr�ation d'une nouvelle liste de sportifs. cr�e une liste vide si la liste en param�tre est
   * vide.
   *
   * @param listeSportifs liste de sportifs vide ou non.
   */
  public ListeSportifs(ArrayList<Sportif> listeSportifs) {
    if (listeSportifs == null) {
      this.listeSportifs = new ArrayList<Sportif>();
    } else {
      this.listeSportifs = listeSportifs;
    }

  }

  /**
   * Ajoute un sportif a la liste. Cet ajout verifie que les parametres ne sont pas null. et que
   * le sportif que l on veut ajouter n a pas le meme pseudo qu un autre sportif.
   *
   * @param id identifiant du sportif
   * @param nom nom du sportif (non null, non vide et pas de chiffre)
   * @param prenom prenom du sportif (non null, non vide et pas de chiffre)
   * @param pseudo pseudo du sportif (non null, non vide et pas le meme qu un autre sportif)
   * @param motDePasse mot de passe du sportif (contrainte similaire au constructeur Sportif)
   * @param dateDeNaissance date de naissance du sportif (non null, inferieur a la date d aujourdhui
   * @param activiteSportive un string qui doit contenir : SKI, BADMINTON, SKATEBOARD ou NATATION
   * @return Vrai si la methode a bien pu ajouter le sportif a la liste
   */
  public boolean ajoutSportif(int id, String nom, String prenom, String pseudo, String motDePasse,
      LocalDate dateDeNaissance, String activiteSportive) {
    Sportif sportif = null;
    try {
      switch (activiteSportive) {
        case "SKI":
          sportif =
              new Sportif(id, nom, prenom, pseudo, motDePasse, dateDeNaissance, SportType.SKI);
          break;
        case "BADMINTON":
          sportif = new Sportif(id, nom, prenom, pseudo, motDePasse, dateDeNaissance,
              SportType.BADMINTON);
          break;
        case "SKATEBOARD":
          sportif = new Sportif(id, nom, prenom, pseudo, motDePasse, dateDeNaissance,
              SportType.SKATEBOARD);
          break;
        case "NATATION":
          sportif =
              new Sportif(id, nom, prenom, pseudo, motDePasse, dateDeNaissance, SportType.NATATION);
          break;
        default:
          return false;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (!this.isOK(sportif)) {
      System.out.println(sportif.getNom());
      return false;
    }
    for (Sportif sp : listeSportifs) {
      if (sp.getPseudo().equals(sportif.getPseudo())) {
        return false;
      }
    }
    return this.listeSportifs.add(sportif);
  }

  /**
   * Verifie si le sportif en parametre n'a pas de valeur null.
   *
   * @param sportif le sportif a verifier
   * @return vrai s'il n'y a pas de valeur null, faux sinon
   */
  private boolean isOK(Sportif sportif) {
    if (sportif.getNom() == null || sportif.getPrenom() == null || sportif.getPseudo() == null
        || sportif.getDateDeNaissance() == null || sportif.getMotDePasse() == null
        || sportif.getActiviteSportive() == null) {
      return false;
    }
    return true;
  }

  /**
   * supprime un sportif a la liste.
   *
   * @param sportif le sportif a supprimer
   * @return Vrai si la methode a bien pu ajouter le sportif a la liste
   */
  public boolean supprimerSportif(Sportif sportif) {
    return this.listeSportifs.remove(sportif);
  }

  public ArrayList<Sportif> getListeSportifs() {
    return this.listeSportifs;
  }

  /**
   * Change la liste de sportifs avec une autre passe en parametre.
   *
   * @param listeSportifs la nouvelle liste de sportifs non null
   */
  public void setListeSportifs(ArrayList<Sportif> listeSportifs) {
    if (listeSportifs != null) {
      this.listeSportifs = listeSportifs;
    }
  }
}
