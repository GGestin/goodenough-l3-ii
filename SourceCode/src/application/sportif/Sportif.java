package application.sportif;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Sportif {
  private StringProperty nom;
  private StringProperty prenom;
  private StringProperty pseudo;
  private StringProperty motDePasse;
  private LocalDate dateDeNaissance;
  private SportType activiteSportive;
  private int id;

  /**
   * constructeur par defaut.
   */
  public Sportif() {}

  /**
   * Creation d'un sportifs avec un mot de passe en MD5.
   *
   * @param nom son nom (non null, au moins 2 caracteres et sans chiffre)
   * @param prenom son prenom (non null, au moins 2 caracteres et sans chiffre)
   * @param pseudo son peusdo (non null est compris entre 1-15 caracteres)
   * @param motDePasse son mot de passe (non null, non vide et au moins 8 caracteres)
   * @param dateDeNaissance sa date de naissance de type Date (non null et inferieur a aujourd'hui)
   * @param activiteSportive son activite sportive (non null)
   */
  public Sportif(int id, String nom, String prenom, String pseudo, String motDePasse,
      LocalDate dateDeNaissance, SportType activiteSportive) throws NoSuchAlgorithmException {

    this.id = id;

    if (nom != null && nom.length() > 1) {
      char[] chars = nom.toCharArray();
      int numberVerifTaille = 0;
      for (char c : chars) {
        if (!Character.isDigit(c)) {
          numberVerifTaille++;
        }
      }
      if (numberVerifTaille == nom.length()) {
        this.nom = new SimpleStringProperty(nom);
      }
    }

    if (prenom != null && prenom.length() > 1) {
      char[] chars2 = prenom.toCharArray();
      int numberVerifTaille2 = 0;
      for (char c : chars2) {
        if (!Character.isDigit(c)) {
          numberVerifTaille2++;
        }
      }
      if (numberVerifTaille2 == prenom.length()) {
        this.prenom = new SimpleStringProperty(prenom);
      }
    }

    if (pseudo != null && pseudo.length() <= 15 && pseudo.length() > 0) {
      this.pseudo = new SimpleStringProperty(pseudo);
    }


    if (motDePasse != null && motDePasse != "" && motDePasse.length() > 7) {
      MessageDigest md = MessageDigest.getInstance("MD5");
      byte[] hashInBytes = md.digest(motDePasse.getBytes(StandardCharsets.UTF_8));
      StringBuilder sb = new StringBuilder();
      for (byte b : hashInBytes) {
        sb.append(String.format("%02x", b));
      }
      this.motDePasse = new SimpleStringProperty(sb.toString());
    }

    LocalDate actuelle = LocalDate.now();
    if (dateDeNaissance != null && actuelle.isAfter(dateDeNaissance)) {
      this.dateDeNaissance = dateDeNaissance;
    }

    if (activiteSportive != null) {
      this.activiteSportive = activiteSportive;
    }

  }

  /**
   * affichage des valeurs dans la liste.
   *
   * @return le pseudo de chaque sportif avec leur nom et prenom.
   */
  public StringProperty afficheListe() {
    String val = this.getPseudo() + " - " + this.getNom() + " " + this.getPrenom();
    return new SimpleStringProperty(val);
  }

  public StringProperty getNomProperty() {
    return this.nom;
  }

  public StringProperty getPrenomProperty() {
    return this.prenom;
  }

  public StringProperty getPseudoProperty() {
    return this.pseudo;
  }

  public StringProperty getMotDePasseProperty() {
    return this.motDePasse;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  /**
   * recupere le nom du sportif.
   *
   * @return le nom du sportif NON NULL
   */
  public String getNom() {
    if (this.nom != null) {
      return this.nom.get();
    }
    return null;
  }

  /**
   * recupere le prenom du sportif.
   *
   * @return le prenom du sportif NON NULL
   */
  public String getPrenom() {
    if (this.prenom != null) {
      return this.prenom.get();
    }
    System.out.println("ici");
    return null;
  }

  /**
   * recupere le prenom et le nom du sportif.
   *
   * @return le prenom et le nom du sportif NON NULL
   */
  public String getPrenomNom() {
    return this.prenom.get() + " " + this.nom.get();
  }

  /**
   * recupere le pseudo du sportif.
   *
   * @return le pseudo du sportif NON NULL
   */
  public String getPseudo() {
    if (this.pseudo != null) {
      return this.pseudo.get();
    }
    return null;
  }

  /**
   * recupere le mot de passe du sportif.
   *
   * @return le mot de passe du sportif NON NULL
   */
  public String getMotDePasse() {
    if (this.motDePasse != null) {
      return this.motDePasse.get();
    }
    System.out.println("ici");
    return null;
  }

  public LocalDate getDateDeNaissance() {
    return this.dateDeNaissance;
  }

  public SportType getActiviteSportive() {
    return this.activiteSportive;
  }

  public void setNomProperty(StringProperty nom) {
    this.nom = nom;
  }

  public void setPrenomProperty(StringProperty prenom) {
    this.prenom = prenom;
  }

  public void setPseudoProperty(StringProperty pseudo) {
    this.pseudo = pseudo;
  }

  public void setMotDePasseProperty(StringProperty motDePasse) {
    this.motDePasse = motDePasse;
  }

  /**
   * Change le nom du sportif avec celui en parametre. prenom non null, superieur a 2 caractere et
   * sans chiffre
   *
   * @param nom le nouveau nom du sportif (non null, au moins 2 caracteres et sans chiffre)
   */
  public void setNom(String nom) {
    if (nom != null && nom.length() > 1) {
      char[] chars = nom.toCharArray();
      int numberVerifTaille = 0;
      for (char c : chars) {
        if (!Character.isDigit(c)) {
          numberVerifTaille++;
        }
      }
      if (numberVerifTaille == nom.length()) {
        this.nom.set(nom);
      }
    }
  }

  /**
   * Change le prenom du sportif avec celui en parametre. prenom non null, superieur a 2 caractere
   * et sans chiffre
   *
   * @param prenom le nouveau prenom du sportif (non null, au moins 2 caracteres et sans chiffre)
   */
  public void setPrenom(String prenom) {
    if (prenom != null && prenom.length() > 1) {
      char[] chars2 = prenom.toCharArray();
      int numberVerifTaille2 = 0;
      for (char c : chars2) {
        if (!Character.isDigit(c)) {
          numberVerifTaille2++;
        }
      }
      if (numberVerifTaille2 == prenom.length()) {
        this.prenom.set(prenom);
      }
    }
  }

  /**
   * Change le pseudo du sportif avec celui en parametre. mot de passe non null, et au plus 15
   * caractere
   *
   * @param pseudo le nouveau sportif du sportif
   */
  public void setPseudo(String pseudo) {
    if (pseudo != null && pseudo.length() <= 15 && pseudo.length() > 0) {
      this.pseudo.set(pseudo);
    }
  }

  /**
   * Change le mot de passe du sportif avec celui en parametre. Le prochain mot de passe sera Hashe
   * avec MD5, et au moins 8 caracteres
   *
   * @param motDePasse le nouveau mot de passe
   */
  public void setMotDePasse(String motDePasse) throws NoSuchAlgorithmException {
    if (motDePasse != null && motDePasse != "" && motDePasse.length() > 7) {
      MessageDigest md = MessageDigest.getInstance("MD5");
      byte[] hashInBytes = md.digest(motDePasse.getBytes(StandardCharsets.UTF_8));
      StringBuilder sb = new StringBuilder();
      for (byte b : hashInBytes) {
        sb.append(String.format("%02x", b));
      }
      this.motDePasse = new SimpleStringProperty(sb.toString());
    }
  }

  /**
   * Change la date de naissance du sportif avec celui en parametre. La date de naissance non null
   * et avant la date d aujourd hui
   *
   * @param dateDeNaissance la nouvelle date de naissance du sportif
   */
  public void setDateDeNaissance(LocalDate dateDeNaissance) {
    LocalDate actuelle = LocalDate.now();
    if (dateDeNaissance != null && actuelle.isAfter(dateDeNaissance)) {
      this.dateDeNaissance = dateDeNaissance;
    }

  }

  /**
   * change l'activite sportive du sportif avec celui en parametre. Activite sportive non null
   *
   * @param activiteSportive la nouvelle activite sportive du sportif
   */
  public void setActiviteSportive(SportType activiteSportive) {
    if (activiteSportive != null) {
      this.activiteSportive = activiteSportive;
    }
  }

  /**
   * Affiche un sportif caracterise par ses elements : nom, prenom. Le mot de passe est bien
   * evidement pas affiche.
   *
   * @return le string contenant les elements du sportifs
   */
  public String toString() {
    String res = this.nom.get() + " " + this.prenom.get();
    return res;
  }

}
