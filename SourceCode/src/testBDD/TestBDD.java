package testBDD;

import java.sql.*;
//import java.util.Calendar;

public class TestBDD {

  private static void affiche(String message) {
    System.out.println(message);
  }

  private static void arret(String message) {
    System.err.println(message);
    System.exit(99);
  }

  public static void main(java.lang.String[] args) {
    Connection con = null;
    ResultSet r�sultats = null;
    String requete = "";

    // chargement du pilote
    try {
      Class.forName("com.mysql.jdbc.Driver");
    } catch (ClassNotFoundException e) {
      arret("Impossible de charger le pilote jdbc");
    }

    // connection a la base de donn�es

    affiche("connexion a la base de donn�es");
    try {

      con = DriverManager.getConnection("jdbc:mysql://eu-cdbr-west-03.cleardb.net/heroku_915839e765a87e5", "b94214e5df8370", "9b6ff569");
    } catch (SQLException e) {
      System.out.println("SQLException: " + e.getMessage());
      System.out.println("SQLState: " + e.getSQLState());
      System.out.println("VendorError: " + e.getErrorCode());
      arret("Connection � la base de donn�es impossible");
    }



    /*
    // insertion d'un enregistrement dans la table client
    affiche("creation enregistrement");

    requete = "DELETE FROM sportif WHERE idSportif = ?";
    try {
      PreparedStatement stmt = con.prepareStatement(requete);
      stmt.setInt(1, 41);

      stmt.execute();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    */


    // creation et execution de la requete
    affiche("creation et execution de la requ�te");
    requete = "SELECT * FROM question";

    try {
      Statement stmt = con.createStatement();
      r�sultats = stmt.executeQuery(requete);
    } catch (SQLException e) {
      arret("Anomalie lors de l'execution de la requ�te");
    }

    // parcours des donn�es retourn�es
    affiche("parcours des donn�es retourn�es");
    try {
      ResultSetMetaData rsmd = r�sultats.getMetaData();
      int nbCols = rsmd.getColumnCount();
      boolean encore = r�sultats.next();

      while (encore) {

        for (int i = 1; i <= nbCols; i++)
          System.out.print(r�sultats.getString(i) + " ");
        System.out.println();
        encore = r�sultats.next();
      }

      r�sultats.close();
    } catch (SQLException e) {
      arret(e.getMessage());
    }

    affiche("fin du programme");
    System.exit(0);
  }
}
