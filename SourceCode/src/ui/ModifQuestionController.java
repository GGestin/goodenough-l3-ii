package ui;

import application.questionnaire.Question;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ModifQuestionController {

  @FXML
  private Text mofif;

  @FXML
  private Label intitule;

  @FXML
  private TextArea intituleQuestion;

  @FXML
  private ChoiceBox<String> reponseDefaut;

  private Question question;
  private int idQuestionnaire;

  private App mainApp;

  private Stage modifQuestionStage;
  private boolean okClicked;

  /**
   * Initialiser la classe controleur. Cette methode est automatiquement appele apres que le fichier
   * fxml ait ete charge. La fonction n'est pas utilis� mais obligatoire.
   */
  @FXML
  private void initialize() {}

  /**
   * Initialise la variable question et le TextArea intituleQuestion avec l'intitul� de la question.
   *
   * @param question la variable qui initialise question
   */
  public void setQuestion(Question question) {
    this.question = question;
    this.reponseDefaut.getItems().setAll(FXCollections.observableArrayList("Non", "Oui"));
    if (!(question.getTexteProperty() == null)) {
      this.intituleQuestion.setText(question.getTexteQuestion());
      this.reponseDefaut.getSelectionModel().select(this.question.getReponseDefaut());
    } else {
      this.mofif.setText("Ajouter une question");
      this.reponseDefaut.getSelectionModel().select(0);
    }

  }



  /**
   * Est appel� quand l'utilisateur appuie sur le bouton OK La fonction initialise l'intitul� de la
   * question avec la valeur indiqu� dans le TextArea intituleQuestion.
   */
  @FXML
  private void handleOk() {
    if (isInputValid()) {

      Connection con = mainApp.getGestionSportifs().connexionBaseDeDonnees();
      String requete;

      if (this.question.getTexteQuestion() == null) {
        // changement en base de donnees
        requete =
            "INSERT INTO question (textQuestion, reponseDefaut, leQuestionnaire) VALUES (?, ?, ?)";

        try {
          PreparedStatement stmt = con.prepareStatement(requete);
          stmt.setString(1, this.intituleQuestion.getText());
          stmt.setInt(2, 0);
          stmt.setInt(3, this.idQuestionnaire);
          stmt.execute();

          System.out.println("test : " + this.intituleQuestion.getText());
          this.question.setTexteQuestion(this.intituleQuestion.getText());

          String req = "SELECT idQuestion FROM question WHERE textQuestion = ?";
          ResultSet resultats = null;

          try {
            PreparedStatement pstmt = con.prepareStatement(req);
            pstmt.setString(1, this.intituleQuestion.getText());
            resultats = pstmt.executeQuery();
          } catch (SQLException exception) {
            System.err.println("Anomalie lors de l'execution de la requ�te "
                + "SELECT idQuestion FROM questionnaire WHERE textQuestion = ?");
          }

          resultats.next();
          this.question.setId(resultats.getInt(1));

          resultats.close();

          this.okClicked = true;

        } catch (SQLException e) {
          e.printStackTrace();
        }
      } else {
        requete = "UPDATE question SET textQuestion = ? WHERE idQuestion = ?";

        try {
          PreparedStatement stmt = con.prepareStatement(requete);
          stmt.setString(1, this.intituleQuestion.getText());
          stmt.setInt(2, this.question.getId());
          stmt.execute();

          System.out.println("test : " + this.intituleQuestion.getText());
          this.question.setTexteQuestion(this.intituleQuestion.getText());
          this.okClicked = true;

        } catch (SQLException e) {
          e.printStackTrace();
        }
      }


      modifQuestionStage.close();
    }
  }

  /**
   * verification des textField.
   *
   * @return true si les inputs sont valides
   */
  private boolean isInputValid() {
    String errorMessage = "";

    if (this.intituleQuestion.getText() == null || this.intituleQuestion.getText().length() == 0) {
      errorMessage += "question invalide !\n";
    }

    if (errorMessage.length() == 0) {
      return true;
    } else {
      // Show the error message.
      Alert alert = new Alert(AlertType.ERROR);
      alert.initOwner(modifQuestionStage);
      alert.setTitle("champ(s) invalide(s)");
      alert.setHeaderText("veuillez rectifier vos informations");
      alert.setContentText(errorMessage);

      alert.showAndWait();

      return false;
    }
  }

  /**
   * Cette fonction est appele avec le Main pour donner sa reference.
   *
   * @param mainApp application Main.
   */
  public void setMainApp(App mainApp) {
    this.mainApp = mainApp;

  }

  /**
   * defini le stage de la fenetre.
   *
   * @param modifQuestionStage le stage a definir pour ce controller
   */
  public void setDialogStage(Stage modifQuestionStage) {
    this.modifQuestionStage = modifQuestionStage;
  }

  /**
   * retourne true si l'utilisateur clique sur OK, sinon false.
   *
   * @return le boolean qui verifie si l'utilisateur a clique sur ok
   */
  public boolean isOkClicked() {
    return this.okClicked;
  }

  public int getIdQuestionnaire() {
    return idQuestionnaire;
  }

  public void setIdQuestionnaire(int idQuestionnaire) {
    this.idQuestionnaire = idQuestionnaire;
  }

}
