package ui;

import application.questionnaire.Question;
import application.questionnaire.Questionnaire;
import application.questionnaire.Reponses;
import application.sportif.Sportif;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Callback;



public class ReponseController {

  public class Replabel {
    private StringProperty label;

    public Replabel(String n) {
      this.label = new SimpleStringProperty(n);
    }

    public StringProperty getLabel() {
      return label;
    }

    public void setLabel(StringProperty label) {
      this.label = label;
    }

  }

  @FXML
  private TableView<Question> questionTable;
  @FXML
  private TableView<Replabel> reponseTable;
  @FXML
  private TableColumn<Question, String> columnQuestion;

  @FXML
  private TableColumn<Replabel, String> columnReponse;

  @FXML
  private TableColumn<Sportif, String> columnJoueur;


  @FXML
  private TableColumn<Question, String> columnSuppr;

  @FXML
  private Label labelARepondu;

  @FXML
  private ChoiceBox<String> menuSportif;

  @FXML
  private ChoiceBox<String> menuQuestionnaire;

  @FXML
  private DatePicker date;

  private ObservableList<Question> questionData = FXCollections.observableArrayList();
  private ObservableList<Replabel> reponseData = FXCollections.observableArrayList();
  private Questionnaire questionnaire = null;
  private Sportif sportif = null;

  @FXML
  private App mainApp;

  @FXML
  private void initialize() {}

  /**
   * Permet d'initialiser le tableau de question et de reponse et l'acc�s � l'application
   * principale.
   *
   * @param mainApp l'application principale
   */
  public void setMainApp(App mainApp) {
    this.mainApp = mainApp;
    ArrayList<String> titresQuestionnaire = new ArrayList<String>();
    ArrayList<String> titresSportif = new ArrayList<String>();
    for (int i = 0; i < mainApp.getGestionSportifs().getQuestionnaires().getListeQuestionnaires()
        .size(); i++) {
      titresQuestionnaire.add(mainApp.getGestionSportifs().getQuestionnaires()
          .getListeQuestionnaires().get(i).getTitre());
    }
    this.menuQuestionnaire.getItems().setAll(titresQuestionnaire);

    for (int i = 0; i < mainApp.getGestionSportifs().getSportifs().getListeSportifs().size(); i++) {
      titresSportif
          .add(mainApp.getGestionSportifs().getSportifs().getListeSportifs().get(i).getPseudo());
    }
    this.menuSportif.getItems().setAll(titresSportif);

    this.columnQuestion.setCellValueFactory(cellData -> cellData.getValue().getTexteProperty());
    this.columnReponse.setCellValueFactory(cellData -> cellData.getValue().getLabel());
    // this.ajouterBoutonModifier();


  }

  /**
   * V�rifie si les champs sont bien remplis avant de les r�cup�rer.
   *
   * @return true si les inputs sont valides
   */
  private boolean isInputValid() {
    String errorMessage = "";

    if (this.menuSportif.getValue() == null || this.menuSportif.getValue().length() == 0) {
      errorMessage += "Sportif invalide !\n";
    }

    if (this.menuQuestionnaire.getValue() == null
        || this.menuQuestionnaire.getValue().length() == 0) {
      errorMessage += "Questionnaire invalide !\n";
    }

    if (date.getValue() == null || date.getValue().isAfter(LocalDate.now())) {
      errorMessage += "Date invalide !\n";
    }

    if (errorMessage.length() == 0) {
      return true;
    } else {
      // Show the error message.
      Alert alert = new Alert(AlertType.ERROR);
      alert.setTitle("champ(s) invalide(s)");
      alert.setHeaderText("veuillez rectifier vos informations");
      alert.setContentText(errorMessage);

      alert.showAndWait();

      return false;
    }
  }

  /**
   * Permet d'afficher les reponses du sportif au questionnaire selectionner dans les choicebox.
   */
  @FXML
  public void handleAfficher() {
    if (this.isInputValid()) {

      if (this.reponseData != null) {
        this.reponseData.clear();
      }
      // recup�ration du questionnaire
      for (int i = 0; i < mainApp.getGestionSportifs().getQuestionnaires().getListeQuestionnaires()
          .size(); i++) {
        if (i == this.menuQuestionnaire.getSelectionModel().getSelectedIndex()) {
          this.questionnaire =
              mainApp.getGestionSportifs().getQuestionnaires().getListeQuestionnaires().get(i);
          break;
        }
      }

      // r�cup�ration du sportif
      for (int j = 0; j < mainApp.getGestionSportifs().getSportifs().getListeSportifs()
          .size(); j++) {
        if (j == this.menuSportif.getSelectionModel().getSelectedIndex()) {
          this.sportif = mainApp.getGestionSportifs().getSportifs().getListeSportifs().get(j);
          break;
        }
      }

      // r�cup�ration Reponses
      int nozero = 0;

      for (int k = 0; k < mainApp.getGestionSportifs().getReponses().getListeReponses()
          .size(); k++) {

        // set de LocalDate en calendar
        LocalDate dbdd =
            mainApp.getGestionSportifs().getReponses().getListeReponses().get(k).getDateReponses();
        Calendar calendarBdd = Calendar.getInstance();
        calendarBdd.clear();
        calendarBdd.set(dbdd.getYear(), dbdd.getMonthValue() - 1, dbdd.getDayOfMonth());

        Calendar calendarBox = Calendar.getInstance();
        calendarBox.clear();
        calendarBox.set(date.getValue().getYear(), date.getValue().getMonthValue() - 1,
            date.getValue().getDayOfMonth());

        System.out.println("week bdd : " + calendarBdd.get(Calendar.WEEK_OF_YEAR));
        System.out.println("week box : " + calendarBox.get(Calendar.WEEK_OF_YEAR));
        System.out.println(date.getValue());
        System.out.println(calendarBox.getTime());

        if (mainApp.getGestionSportifs().getReponses().getListeReponses().get(k)
            .getQuestionnaireReponses() == this.questionnaire

            && calendarBdd.get(Calendar.WEEK_OF_YEAR) == calendarBox.get(Calendar.WEEK_OF_YEAR)

            && mainApp.getGestionSportifs().getReponses().getListeReponses().get(k)
                .getSportifReponses() == this.sportif
            && nozero < this.questionnaire.getListeQuestions().size()) {

          System.out
              .println("Questionnaire taille : " + this.questionnaire.getListeQuestions().size());
          System.out.println("taille nozero : " + nozero);

          for (int l = 0; l < mainApp.getGestionSportifs().getReponses().getListeReponses().get(k)
              .getListeReponses().size(); l++) {
            if (mainApp.getGestionSportifs().getReponses().getListeReponses().get(k)
                .getListeReponses().get(l) == 1) {
              this.reponseData.add(new Replabel("oui"));


              nozero++;

            } else if (mainApp.getGestionSportifs().getReponses().getListeReponses().get(k)
                .getListeReponses().get(l) == 0) {

              this.reponseData.add(new Replabel("non"));


              nozero++;
            }
          }
        }

      }
      System.out.println();

      if (nozero == 0) {
        for (int i = 0; i < this.questionnaire.getListeQuestions().size(); i++) {
          if (this.questionnaire.getListeQuestions().get(i).getReponseDefaut() == 0) {
            System.out
                .println("Questionnaire taille : " + this.questionnaire.getListeQuestions().size());
            this.reponseData.add(new Replabel("non"));
          } else {
            this.reponseData.add(new Replabel("oui"));
          }
        }
        this.labelARepondu.setText("PAS REPONDU ! ");
      } else if (nozero != 0) {
        this.labelARepondu.setText("R�pondu");

      }



      // affichage des questions dans le tableau
      questionData.setAll(questionnaire.getListeQuestions());
      // System.out.println(questionnaire.getListeQuestions());
      questionTable.setItems(questionData);

      // affichage des reponses dans le tableau
      this.reponseTable.setItems(reponseData);


      /*
       * System.out.println("test : " + mainApp.getGestionSportifs().getQuestionnaires()
       * .getListeQuestionnaires().get(0).getListeQuestions().get(0).getTextQuestion());
       */
    }

  }

  /**
   * permet de supprimer les reponses au question en les rempla�ant par les reponses par defaut.
   */
  @FXML
  public void handleSupprimer() {

    this.reponseData.clear();
    for (int i = 0; i < this.questionnaire.getListeQuestions().size(); i++) {
      if (this.questionnaire.getListeQuestions().get(i).getReponseDefaut() == 0) {
        System.out
            .println("Questionnaire taille : " + this.questionnaire.getListeQuestions().size());
        this.reponseData.add(new Replabel("non"));
      } else {
        this.reponseData.add(new Replabel("oui"));
      }
    }
    this.labelARepondu.setText("PAS REPONDU ! ");
  }

  /*
   * /** Permet d'ajouter le bouton modifi� au tableau de r�ponse.
   */
  /*
   * private void ajouterBoutonModifier() {
   *
   * TableColumn<Replabel, Void> columnSupprReponse = new TableColumn<Replabel, Void>();
   * columnSupprReponse.setMaxWidth(2000);
   *
   *
   * Callback<TableColumn<Replabel, Void>, TableCell<Replabel, Void>> cellFactory = new
   * Callback<TableColumn<Replabel, Void>, TableCell<Replabel, Void>>() {
   *
   * @Override public TableCell<Replabel, Void> call(TableColumn<Replabel, Void> param) { final
   * TableCell<Replabel, Void> cell = new TableCell<Replabel, Void>() {
   *
   * private final Button btn = new Button("modifier");
   *
   * { btn.setOnAction((ActionEvent event) -> {
   *
   * int index = getIndex(); Reponses rep; for (int i = 0; i <
   * mainApp.getGestionSportifs().getReponses().getListeReponses() .size(); i++) { if
   * (mainApp.getGestionSportifs().getReponses().getListeReponses().get(i)
   * .getQuestionnaireReponses() == questionnaire) { System.out.println(i); } }
   * mainApp.showModifReponseOverview(
   * mainApp.getGestionSportifs().getReponses().getListeReponses().get(index), index,
   * questionnaire); reponseTable.refresh(); }); }
   *
   * @Override public void updateItem(Void item, boolean empty) { super.updateItem(item, empty); if
   * (empty) { setGraphic(null); } else { setGraphic(btn); } } }; return cell; } };
   * columnSupprReponse.setCellFactory(cellFactory);
   * reponseTable.getColumns().add(columnSupprReponse); }
   */
}
