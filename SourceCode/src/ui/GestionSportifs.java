package ui;

import application.questionnaire.ListeQuestionnaires;
import application.questionnaire.ListeReponses;
import application.questionnaire.Questionnaire;
import application.questionnaire.Reponses;
import application.sportif.ListeSportifs;
import application.sportif.Sportif;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class GestionSportifs {

  /**
   * les listes observable pour les sportifs, les questions et les questionnaires.
   */
  private ListeSportifs sportifs;
  private ListeQuestionnaires questionnaires;
  private ListeReponses reponses;

  private ObservableList<Sportif> sportifData = FXCollections.observableArrayList();
  private ObservableList<Questionnaire> questionnaireData = FXCollections.observableArrayList();
  private ObservableList<Reponses> reponsesData = FXCollections.observableArrayList();

  /**
   * controlleur de GestionSportifs qui ajoute quelques sportifs, questionnaires et questions.
   */
  public GestionSportifs() {

    // connexion a la BDD
    Connection con = connexionBaseDeDonnees();

    ResultSet resultats = null;
    String requete = "";


    /* AJOUT DES SPORTIFS */

    // instanciation de la liste des sportifs
    this.sportifs = new ListeSportifs(new ArrayList<Sportif>());

    // creation et execution de la requete
    requete = "SELECT * FROM sportif";

    try {
      Statement stmt = con.createStatement();
      resultats = stmt.executeQuery(requete);
    } catch (SQLException e) {
      System.err.println("Anomalie lors de l'execution de la requ�te SELECT * FROM sportif");
    }

    // faire la requete pour recuperer les sportifs
    try {
      boolean encore = resultats.next();

      while (encore) {
        if (!sportifs.ajoutSportif(resultats.getInt(1), resultats.getString(2),
            resultats.getString(3), resultats.getString(4), resultats.getString(5),
            resultats.getDate(6).toLocalDate(), resultats.getString(7))) {
          System.err.println("erreur d'ajout d'un sportif");
        }
        System.out.println(resultats.getInt(1) + " " + resultats.getString(2) + " "
            + resultats.getString(3) + " " + resultats.getString(4) + " " + resultats.getString(5)
            + " " + resultats.getDate(6).toLocalDate() + " " + resultats.getString(7));
        encore = resultats.next();
      }

      resultats.close();
    } catch (SQLException e) {
      System.err.println(e.getMessage());
    }


    /* AJOUT DES QUESTIONNAIRES */

    // instanciation de la liste des questionnaires
    this.questionnaires = new ListeQuestionnaires(new ArrayList<Questionnaire>());

    resultats = null;
    requete = "";
    ResultSet resQ = null;
    String requete2 = "";

    // creation et execution de la requete
    requete = "SELECT * FROM questionnaire";

    try {
      Statement stmt = con.createStatement();
      resultats = stmt.executeQuery(requete);
    } catch (SQLException e) {
      System.err.println("Anomalie lors de l'execution de la requ�te SELECT * FROM questionnaire");
    }

    // faire la requete pour recuperer les questionnaires
    try {
      boolean encore = resultats.next();
      int ind = 0;

      while (encore) {
        // ajout des questionnaires dans la liste des questionnaires
        if (!questionnaires.ajoutQuestionnaire(resultats.getInt(1), resultats.getString(2),
            resultats.getBoolean(3))) {
          System.err.println("erreur d'ajout d'un questionnaire");
        }

        // ajout des questions du questionnaire
        requete2 = "SELECT * FROM question WHERE leQuestionnaire = ?";

        try {
          PreparedStatement stmt = con.prepareStatement(requete2);
          stmt.setInt(1, resultats.getInt(1));
          resQ = stmt.executeQuery();
        } catch (SQLException e) {
          System.err.println("Anomalie lors de l'execution de la requ�te SELECT * FROM question");
        }

        try {
          boolean again = resQ.next();

          while (again) {

            if (!questionnaires.getListeQuestionnaires().get(ind).ajoutQuestion(resQ.getInt(1),
                resQ.getString(2), resQ.getInt(3))) {
              System.err.println("erreur d'ajout d'une question");
            }
            again = resQ.next();

          }
          resQ.close();
        } catch (SQLException e) {
          System.err.println(e.getMessage());
        }
        ind++;
        encore = resultats.next();
      }

      resultats.close();
    } catch (SQLException e) {
      System.err.println(e.getMessage());
    }

    /* AJOUT DES REPONSES */

    // creation de la liste des reponses de reponses
    this.reponses = new ListeReponses(new ArrayList<Reponses>());

    resultats = null;

    // creation et execution de la requete
    requete = "SELECT * FROM reponses";

    try {
      Statement stmt = con.createStatement();
      resultats = stmt.executeQuery(requete);
    } catch (SQLException e) {
      System.err.println("Anomalie lors de l'execution de la requ�te SELECT * FROM reponses");
    }

    // parcours des donn�es retourn�es
    try {
      boolean encore = resultats.next();
      int ind = 0;
      Sportif sportif;
      Questionnaire questionnaire;

      while (encore) {

        // recherche du sportif
        sportif = null;
        for (Sportif sp : this.sportifs.getListeSportifs()) {
          if (sp.getId() == resultats.getInt(3)) {
            sportif = sp;
          }
        }

        // recherche du questionnaire
        questionnaire = null;
        for (Questionnaire qt : this.questionnaires.getListeQuestionnaires()) {
          if (qt.getId() == resultats.getInt(4)) {
            questionnaire = qt;
          }
        }

        if (!this.reponses.ajoutReponses(new ArrayList<>(), sportif, questionnaire,
            resultats.getDate(2).toLocalDate())) {
          System.err.println("erreur d'ajout d'une question");
        } else {

          ResultSet res = null;
          String req = "SELECT * FROM reponse WHERE lesReponses = ?";

          try {
            PreparedStatement stmt = con.prepareStatement(req);
            stmt.setInt(1, resultats.getInt(1));
            res = stmt.executeQuery();
          } catch (SQLException e) {
            System.err.println("Anomalie lors de l'execution de la requ�te"
                + "SELECT * FROM reponse WHERE lesReponses = ?");
          }

          try {
            boolean again = res.next();

            while (again) {
              System.out
                  .println("id Question : " + res.getInt(4) + ", id reponse : " + res.getInt(1));
              this.reponses.getListeReponses().get(ind).ajoutReponse(res.getInt(2));
              again = res.next();
            }
            res.close();
          } catch (SQLException e) {
            System.err.println(e.getMessage());
          }

        }

        ind++;
        encore = resultats.next();
      }

      resultats.close();
    } catch (SQLException e) {
      System.err.println(e.getMessage());
    }

    // lien entre les listes et les listes observables
    this.lienSportsObservable();
    this.lienQuestionnairesObservable();
    this.lienReponsesObservable();
  }

  /**
   * methode permettant de lier la liste des sportifs a une liste observable de sportifs.
   *
   * @return true si le lien a bien ete fait.
   */

  public boolean lienSportsObservable() {
    for (int i = 0; i < this.sportifData.size(); i++) {
      this.sportifData.remove(i);
    }
    try {
      for (Sportif sp : this.sportifs.getListeSportifs()) {
        this.sportifData.add(sp);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  /**
   * methode permettant de lier la liste des questionnaires a une liste observable de
   * questionnaires.
   *
   * @return true si le lien a bien ete fait.
   */

  public boolean lienQuestionnairesObservable() {
    try {
      for (Questionnaire qt : this.questionnaires.getListeQuestionnaires()) {
        this.questionnaireData.add(qt);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  /**
   * methode permettant de lier la liste des sportifs a une liste observable de sportifs.
   *
   * @return true si le lien a bien ete fait.
   */

  public boolean lienReponsesObservable() {
    try {
      for (Reponses rp : this.reponses.getListeReponses()) {
        this.reponsesData.add(rp);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  /**
   * retourne les sportifs en liste observable.
   *
   * @return un liste observable de sportif
   */
  public ListeSportifs getSportifs() {
    return this.sportifs;
  }

  /**
   * retourne les questions en liste observable.
   *
   * @return un liste observable de questions
   */
  public ListeQuestionnaires getQuestionnaires() {
    return this.questionnaires;
  }

  /**
   * retourne les sportifs en liste observable.
   *
   * @return un liste observable de sportif
   */
  public ObservableList<Sportif> getSportifData() {
    return this.sportifData;
  }

  /**
   * retourne les questions en liste observable.
   *
   * @return un liste observable de questions
   */
  public ObservableList<Questionnaire> getQuestionnaireData() {
    return this.questionnaireData;
  }

  /**
   * retourne la liste des Reponses.
   *
   * @return la liste des reponses
   */
  public ListeReponses getReponses() {
    return this.reponses;
  }

  /**
   * retourne les reponses en listeObservable.
   *
   * @return un liste observable de reponses
   */
  public ObservableList<Reponses> getReponsesData() {
    return this.reponsesData;
  }

  /**
   * Fonction de connexion a la base de donnees.
   */
  public Connection connexionBaseDeDonnees() {
    Connection con = null;

    // chargement du pilote
    try {
      Class.forName("com.mysql.jdbc.Driver");
    } catch (ClassNotFoundException e) {
      System.err.println("Impossible de charger le pilote jdbc");
    }

    // connection a la base de donn�es

    System.out.println("connexion a la base de donn�es");
    try {

      con = DriverManager.getConnection(
          "jdbc:mysql://eu-cdbr-west-03.cleardb.net/heroku_915839e765a87e5", "b94214e5df8370",
          "9b6ff569");
    } catch (SQLException e) {
      System.out.println("SQLException: " + e.getMessage());
      System.out.println("SQLState: " + e.getSQLState());
      System.out.println("VendorError: " + e.getErrorCode());
      System.out.println("Connection � la base de donn�es impossible");
    }
    return con;
  }



  /*
   * retourne les questionnaires en liste observable.
   *
   * @return un liste observable de questionnaires
   *
   * public ListeReponse getReponses() { return this.reponses; }
   */

}
