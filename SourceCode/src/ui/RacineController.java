package ui;

import javafx.fxml.FXML;

public class RacineController {

  private App mainApp;

  @FXML
  private void handleVueSportif() {
    mainApp.showListeSportifOverview();
  }

  @FXML
  private void handleVueQuestionnaire() {
    mainApp.showListeQuestionnaireOverview();
  }

  @FXML
  private void handleVueReponses() {
    mainApp.showReponseOverview();
  }

  public void setMainApp(App mainApp) {
    this.mainApp = mainApp;
  }
}
