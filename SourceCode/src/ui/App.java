package ui;

import application.questionnaire.Question;
import application.questionnaire.Questionnaire;
import application.questionnaire.Reponses;
import application.sportif.Sportif;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class App extends Application {

  private Stage premierStage;
  private BorderPane racine;

  private GestionSportifs gs;

  /**
   * Constructeur.
   */
  public App() {
    this.gs = new GestionSportifs();
  }

  public GestionSportifs getGestionSportifs() {
    return this.gs;
  }

  @Override
  public void start(Stage premierStage) {
    this.premierStage = premierStage;
    this.premierStage.setTitle("Application Sportif SportEnough");

    initRootLayout();
    showListeSportifOverview();
    // showSportifOverview(this.gs.getSportifs().getListeSportifs().get(0));
  }

  /**
   * Initialise l'affichage de la racine.
   */
  public void initRootLayout() {
    try {
      // Load root layout from fxml file.
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(App.class.getResource("Racine.fxml"));
      racine = (BorderPane) loader.load();

      // Show the scene containing the root layout.
      Scene scene = new Scene(racine);
      premierStage.setScene(scene);
      premierStage.show();

      // Give the controller access to the main app.
      RacineController controller = loader.getController();
      controller.setMainApp(this);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * affiche la vue de la liste des questionnaires.
   */
  public void showListeQuestionnaireOverview() {
    try {
      // Load person overview.
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(App.class.getResource("ListeQuestionnaire.fxml"));
      AnchorPane listeQuestionnaireOverview = (AnchorPane) loader.load();

      // Set person overview into the center of root layout.
      racine.setCenter(listeQuestionnaireOverview);

      // Give the controller access to the main app.
      ListeQuestionnaireController controller = loader.getController();
      controller.setMainApp(this);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * affiche la vue de la liste des sportifs.
   */
  public void showListeSportifOverview() {
    try {
      // Load person overview.
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(App.class.getResource("ListeSportif.fxml"));
      AnchorPane listeSportifOverview = (AnchorPane) loader.load();

      // Set person overview into the center of root layout.
      racine.setCenter(listeSportifOverview);

      // Give the controller access to the main app.
      ListeSportifController controller = loader.getController();
      controller.setMainApp(this);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * affiche la vue de la liste des sportifs.
   */
  public void showReponseOverview() {
    try {
      // Load person overview.
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(App.class.getResource("Reponse.fxml"));
      AnchorPane listeSportifOverview = (AnchorPane) loader.load();

      // Set person overview into the center of root layout.
      racine.setCenter(listeSportifOverview);

      // Give the controller access to the main app.
      ReponseController controller = loader.getController();
      controller.setMainApp(this);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Fonction pour afficher la vue de modification d'une reponse.
   *
   * @param reponse la reponse (boolean).
   * @param indexQuestion l'index de la question pour avoir les reponses dans l'ordre.
   * @param questionnaire questionnaire de la reponse
   * @return true la fonction c'est bien deroule.
   */

  public boolean showModifReponseOverview(Reponses reponse, int indexQuestion,
      Questionnaire questionnaire) {
    try {
      // Load person overview.
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(App.class.getResource("ModifReponse.fxml"));
      AnchorPane modifReponseOverview = (AnchorPane) loader.load();

      // Create the dialog Stage.
      Stage modifReponseStage = new Stage();
      modifReponseStage.setTitle("Question");
      modifReponseStage.initModality(Modality.WINDOW_MODAL);
      modifReponseStage.initOwner(premierStage);
      Scene scene = new Scene(modifReponseOverview);
      modifReponseStage.setScene(scene);

      // Give the controller access to the main app.
      ModifReponseController controller = loader.getController();
      controller.setDialogStage(modifReponseStage);
      controller.setReponse(reponse, indexQuestion, questionnaire);
      controller.setMainApp(this);

      // Show the dialog and wait until the user closes it
      modifReponseStage.showAndWait();

      return controller.isOkClicked();

    } catch (IOException e) {
      e.printStackTrace();
      return false;
    }
  }

  /**
   * affiche la vue Questionnaire.
   *
   * @param indexQuestionnaire l'index de questionnaire que l'on veut afficher.
   */
  public void showQuestionnaireOverview(int indexQuestionnaire) {
    try {
      // Load person overview.
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(App.class.getResource("Questionnaire.fxml"));
      AnchorPane questionnaireOverview = (AnchorPane) loader.load();

      // Set person overview into the center of root layout.
      racine.setCenter(questionnaireOverview);

      // Give the controller access to the main app.
      QuestionnaireController controller = loader.getController();
      controller.setMainApp(this);
      controller.setIndexQuestionnaire(indexQuestionnaire);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * affiche la vue ModifQuestion.
   *
   * @param question la question qu'on veut modifier (ou ajouter)
   * @return true si Ok a �t� cliqu� false sinon
   */
  public boolean showModifQuestionOverview(Question question, int idQuestionnaire) {
    try {
      // Load person overview.
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(App.class.getResource("ModifQuestion.fxml"));
      AnchorPane modifQuestionOverview = (AnchorPane) loader.load();

      // Create the dialog Stage.
      Stage modifQuestionStage = new Stage();
      modifQuestionStage.setTitle("Question");
      modifQuestionStage.initModality(Modality.WINDOW_MODAL);
      modifQuestionStage.initOwner(premierStage);
      Scene scene = new Scene(modifQuestionOverview);
      modifQuestionStage.setScene(scene);

      // Give the controller access to the main app.
      ModifQuestionController controller = loader.getController();
      controller.setDialogStage(modifQuestionStage);
      controller.setQuestion(question);
      controller.setIdQuestionnaire(idQuestionnaire);
      controller.setMainApp(this);

      // Show the dialog and wait until the user closes it
      modifQuestionStage.showAndWait();

      return controller.isOkClicked();

    } catch (IOException e) {
      e.printStackTrace();
      return false;
    }
  }

  /**
   * affiche la vue AjoutQuestionnaire.
   *
   * @param questionnaire le questionnaire qu'on veut ajouter
   * @return true si Ok a �t� cliqu� false sinon
   */
  public boolean showAjoutQuestionnaireOverview(Questionnaire questionnaire) {
    try {
      // Load person overview.
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(App.class.getResource("AjoutQuestionnaire.fxml"));
      AnchorPane ajoutQuestionnaireControllerOverview = (AnchorPane) loader.load();

      // Create the dialog Stage.
      Stage ajoutQuestionnaireStage = new Stage();
      ajoutQuestionnaireStage.setTitle("Questionnaire");
      ajoutQuestionnaireStage.initModality(Modality.WINDOW_MODAL);
      ajoutQuestionnaireStage.initOwner(premierStage);
      Scene scene = new Scene(ajoutQuestionnaireControllerOverview);
      ajoutQuestionnaireStage.setScene(scene);

      // Give the controller access to the main app.
      AjoutQuestionnaireController controller = loader.getController();
      controller.setDialogStage(ajoutQuestionnaireStage);
      controller.setQuestionnaire(questionnaire);
      controller.setMainApp(this);

      // Show the dialog and wait until the user closes it
      ajoutQuestionnaireStage.showAndWait();

      return controller.isOkClicked();

    } catch (IOException e) {
      e.printStackTrace();
      return false;
    }
  }

  /**
   * afficher la vue du sportif.
   *
   * @param sportif selectionne dans la liste des sportifs qu'on utilise pour afficher ses donnees
   * @return true si Ok a �t� cliqu� false sinon
   */
  public boolean showSportifOverview(Sportif sportif) {
    try {
      // Load person overview.
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(App.class.getResource("Sportif.fxml"));
      AnchorPane sportifOverview = (AnchorPane) loader.load();

      // Create the dialog Stage.
      Stage sportifStage = new Stage();
      sportifStage.setTitle("Sportif");
      sportifStage.initModality(Modality.WINDOW_MODAL);
      sportifStage.initOwner(premierStage);
      Scene scene = new Scene(sportifOverview);
      sportifStage.setScene(scene);

      // Give the controller access to the main app.
      SportifController controller = loader.getController();
      controller.setDialogStage(sportifStage);
      controller.setSportif(sportif);
      controller.setMainApp(this);

      // Show the dialog and wait until the user closes it
      sportifStage.showAndWait();

      return controller.isOkClicked();

    } catch (IOException e) {
      e.printStackTrace();
      return false;
    }
  }

  /**
   * afficher la vue du sportif dans le root layout.
   */
  public void showAdminOverview() {
    try {
      // Load person overview.
      FXMLLoader loader = new FXMLLoader();
      loader.setLocation(App.class.getResource("Admin.fxml"));
      AnchorPane adminOverview = (AnchorPane) loader.load();

      // Set person overview into the center of root layout.
      racine.setCenter(adminOverview);

      // Give the controller access to the main app.
      ListeSportifController controller = loader.getController();
      controller.setMainApp(this);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Retourne la page principale.
   *
   * @return page principale
   */
  public Stage getPrimaryStage() {
    return premierStage;
  }

  public static void main(String[] args) {
    launch(args);
  }
}
