package ui;

import application.questionnaire.Questionnaire;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcons;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.util.Callback;

public class ListeQuestionnaireController {

  @FXML
  private TableView<Questionnaire> questionnaireTableau;
  @FXML
  private TableColumn<Questionnaire, String> questionnaires;

  // Reference a l'application Main.
  private App mainApp;

  /**
   * Le constructeur,appel� avant la methode initialiser().
   */
  public ListeQuestionnaireController() {}

  /**
   * Initialiser la classe controleur. Cette methode est automatiquement appele apres que le fichier
   * fxml ait ete charge.
   */
  @FXML
  private void initialize() {

    questionnaires.setCellValueFactory(cellData -> cellData.getValue().getTitreProperty());
    this.ajouterBoutonModifierTableauQuestionnaire();
    // this.ajouterBoutonStatsTableauQuestionnaire();
    this.ajouterBoutonSupprimerTableauQuestionnaire();
  }

  /**
   * appele quand l'utilisateur appuie sur le bouton "ajouter" d'un questionnaire. ca affiche un
   * dialogue pour rentrer le Titre du Questionnaire.
   */
  @FXML
  private void handleNewQuestionnaire() {
    Questionnaire nouveauQuestionnaire = new Questionnaire(null, false, 0);
    boolean okClicked = mainApp.showAjoutQuestionnaireOverview(nouveauQuestionnaire);
    if (okClicked) {
      mainApp.getGestionSportifs().getQuestionnaires().ajoutQuestionnaire(
          nouveauQuestionnaire.getId(), nouveauQuestionnaire.getTitre(),
          nouveauQuestionnaire.isEstOuvert());
      mainApp.getGestionSportifs().getQuestionnaireData().add(nouveauQuestionnaire);
    }
  }

  /**
   * ajouter du bouton modifier au tableau des questionnaires.
   */
  private void ajouterBoutonModifierTableauQuestionnaire() {
    TableColumn<Questionnaire, Void> colModBtn = new TableColumn<Questionnaire, Void>();
    colModBtn.setMaxWidth(500);

    Callback<TableColumn<Questionnaire, Void>, TableCell<Questionnaire, Void>> cellFactory =
        new Callback<TableColumn<Questionnaire, Void>, TableCell<Questionnaire, Void>>() {
          @Override
          public TableCell<Questionnaire, Void> call(final TableColumn<Questionnaire, Void> param) {
            final TableCell<Questionnaire, Void> cell = new TableCell<Questionnaire, Void>() {

              private final HBox headerBox = new HBox();

              {
                headerBox.getStyleClass().add("header_component");
                headerBox.getChildren()
                    .addAll(GlyphsDude.createIcon(FontAwesomeIcons.PENCIL, "2.5em"));
                headerBox.setOnMouseClicked(e -> {
                  if (e.getButton() == MouseButton.PRIMARY) {
                    Questionnaire questionnaire = getTableView().getItems().get(getIndex());
                    mainApp.showQuestionnaireOverview(questionnaire.getId());
                  }
                });
              }

              @Override
              public void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                  setGraphic(null);
                } else {
                  setGraphic(headerBox);
                }
              }
            };
            return cell;
          }
        };

    colModBtn.setCellFactory(cellFactory);

    questionnaireTableau.getColumns().add(colModBtn);

  }

  /*
   * ajoute du bouton statistiques au tableau des questionnaires.
   */
  /*
   * private void ajouterBoutonStatsTableauQuestionnaire() { TableColumn<Questionnaire, Void>
   * colStatBtn = new TableColumn<Questionnaire, Void>();
   *
   * Callback<TableColumn<Questionnaire, Void>, TableCell<Questionnaire, Void>> cellFactory = new
   * Callback<TableColumn<Questionnaire, Void>, TableCell<Questionnaire, Void>>() {
   *
   * @Override public TableCell<Questionnaire, Void> call(final TableColumn<Questionnaire, Void>
   * param) { final TableCell<Questionnaire, Void> cell = new TableCell<Questionnaire, Void>() {
   *
   * private final Button btn = new Button("Stats");
   *
   * { btn.setOnAction((ActionEvent event) -> { Questionnaire questionnaire =
   * getTableView().getItems().get(getIndex()); System.out.println("statistiques du " +
   * questionnaire.getTitre()); }); }
   *
   * @Override public void updateItem(Void item, boolean empty) { super.updateItem(item, empty); if
   * (empty) { setGraphic(null); } else { setGraphic(btn); } } }; return cell; } };
   *
   * colStatBtn.setCellFactory(cellFactory);
   *
   * questionnaireTableau.getColumns().add(colStatBtn);
   *
   * }
   */

  /**
   * ajoute du bouton supprimer au tableau des questionnaires.
   */
  private void ajouterBoutonSupprimerTableauQuestionnaire() {
    TableColumn<Questionnaire, Void> colDelBtn = new TableColumn<Questionnaire, Void>();
    colDelBtn.setMaxWidth(500);

    Callback<TableColumn<Questionnaire, Void>, TableCell<Questionnaire, Void>> cellFactory =
        new Callback<TableColumn<Questionnaire, Void>, TableCell<Questionnaire, Void>>() {
          @Override
          public TableCell<Questionnaire, Void> call(final TableColumn<Questionnaire, Void> param) {
            final TableCell<Questionnaire, Void> cell = new TableCell<Questionnaire, Void>() {

              private final HBox headerBox = new HBox();

              {
                headerBox.getStyleClass().add("header_component");
                headerBox.getChildren()
                    .addAll(GlyphsDude.createIcon(FontAwesomeIcons.TRASH, "2.5em"));
                headerBox.setOnMouseClicked(e -> {
                  if (e.getButton() == MouseButton.PRIMARY) {
                    Questionnaire questionnaire = getTableView().getItems().get(getIndex());

                    if (confirmationSupprimerQuestionnaire(questionnaire)) {
                      String requete = null;
                      ResultSet resultats = null;

                      // suppression du questionnaire avec connexion a la base de donnees
                      Connection con = mainApp.getGestionSportifs().connexionBaseDeDonnees();

                      /* PARTIE QUESTION */

                      // requete pour recuperer toutes les questions du questionnaire
                      requete = "SELECT idQuestion FROM question WHERE leQuestionnaire = ?";

                      try {
                        PreparedStatement stmt = con.prepareStatement(requete);
                        System.out.println("id du questionnaire : " + questionnaire.getId());
                        stmt.setInt(1, questionnaire.getId());
                        resultats = stmt.executeQuery();
                      } catch (SQLException exception) {
                        System.err.println("Anomalie lors de l'execution de la requ�te "
                            + "SELECT idQuestion FROM question WHERE leQuestionnaire = ?");
                      }

                      // boucle sur chaque question du questionnaire
                      try {
                        boolean encore = resultats.next();

                        while (encore) {
                          ResultSet res = null;

                          // requete pour recuperer chaque reponse de la question
                          String req = "SELECT idReponse FROM reponse WHERE laQuestion = ?";
                          try {
                            PreparedStatement stmt = con.prepareStatement(req);
                            System.out.println("id de la question : " + resultats.getInt(1));
                            stmt.setInt(1, resultats.getInt(1));
                            res = stmt.executeQuery();
                          } catch (SQLException exception) {
                            System.err.println("Anomalie lors de l'execution de la requ�te "
                                + "SELECT idReponse FROM reponse WHERE laQuestion = ?");
                          }

                          // boucle sur chaque reponse de la question
                          try {
                            boolean again = res.next();

                            while (again) {

                              // requete de suppression de chaque reponse de la question
                              String requeteDeleteReponse =
                                  "DELETE FROM reponse WHERE idReponse = ?";

                              try {
                                PreparedStatement stmt = con.prepareStatement(requeteDeleteReponse);
                                stmt.setInt(1, res.getInt(1));
                                stmt.execute();

                              } catch (SQLException e1) {
                                e1.printStackTrace();
                              }
                            }
                            again = res.next();

                          } catch (SQLException exception) {
                            System.err.println(exception.getMessage());
                          }

                          // requete de suppression de chaque question du questionnaire
                          String requeteDeleteQuestion =
                              "DELETE FROM question WHERE idQuestion = ?";

                          try {
                            PreparedStatement stmt = con.prepareStatement(requeteDeleteQuestion);
                            stmt.setInt(1, resultats.getInt(1));
                            stmt.execute();
                            mainApp.getGestionSportifs().getQuestionnaires()
                                .getListeQuestionnaires().get(getIndex()).getListeQuestions()
                                .remove(getIndex());

                          } catch (SQLException e1) {
                            e1.printStackTrace();
                          }
                          encore = resultats.next();
                        }

                        resultats.close();
                      } catch (SQLException exception) {
                        System.err.println(exception.getMessage());
                      }

                      /* PARTIE REPONSES */

                      // requete pour recuperer toutes les listes de reponses du questionnaire
                      requete = "SELECT idReponses FROM reponses WHERE questionnaireReponses = ?";

                      try {
                        PreparedStatement stmt = con.prepareStatement(requete);
                        stmt.setInt(1, questionnaire.getId());
                        resultats = stmt.executeQuery();
                      } catch (SQLException exception) {
                        System.err.println("Anomalie lors de l'execution de la requ�te "
                            + "SELECT idReponses FROM question WHERE questionnaireReponses = ?");
                      }

                      // boucle sur chaque liste de reponses du questionnaire
                      try {
                        boolean encore = resultats.next();

                        while (encore) {
                          ResultSet res = null;

                          // requete pour recuperer chaque reponse de la liste de reponses
                          String req = "SELECT idReponse FROM reponse WHERE lesReponses = ?";
                          try {
                            PreparedStatement stmt = con.prepareStatement(req);
                            stmt.setInt(1, resultats.getInt(1));
                            res = stmt.executeQuery();
                          } catch (SQLException exception) {
                            System.err.println("Anomalie lors de l'execution de la requ�te "
                                + "SELECT idReponse FROM reponse WHERE laQuestion = ?");
                          }

                          // boucle sur chaque reponse de la liste de reponses
                          try {
                            boolean again = res.next();

                            while (again) {

                              // requete de suppression de chaque reponse de la liste de reponses
                              String requeteDeleteReponse =
                                  "DELETE FROM reponse WHERE idReponse = ?";

                              try {
                                PreparedStatement stmt = con.prepareStatement(requeteDeleteReponse);
                                stmt.setInt(1, res.getInt(1));
                                stmt.execute();
                                mainApp.getGestionSportifs().getReponses().getListeReponses()
                                    .get(resultats.getInt(1)).getListeReponses()
                                    .remove(res.getInt(1));


                              } catch (SQLException e1) {
                                e1.printStackTrace();
                              }

                              again = res.next();
                            }
                          } catch (SQLException exception) {
                            System.err.println(exception.getMessage());
                          }

                          // requete de suppression de chaque liste de reponses du questionnaire
                          String requeteDeleteLR = "DELETE FROM reponses WHERE idReponses = ?";

                          try {
                            PreparedStatement stmt = con.prepareStatement(requeteDeleteLR);
                            stmt.setInt(1, resultats.getInt(1));
                            stmt.execute();
                            mainApp.getGestionSportifs().getReponsesData()
                                .remove(resultats.getInt(1));
                            mainApp.getGestionSportifs().getReponses().getListeReponses()
                                .remove(resultats.getInt(1));

                          } catch (SQLException e1) {
                            e1.printStackTrace();
                          }

                          encore = resultats.next();
                        }

                        resultats.close();
                      } catch (SQLException exception) {
                        System.err.println(exception.getMessage());
                      }


                      // requete de suppression du questionnaire
                      requete = "DELETE FROM questionnaire WHERE idQuestionnaire = ?";

                      try {
                        PreparedStatement stmt = con.prepareStatement(requete);
                        stmt.setInt(1, questionnaire.getId());
                        stmt.execute();
                        mainApp.getGestionSportifs().getQuestionnaireData().remove(getIndex());
                        mainApp.getGestionSportifs().getQuestionnaires().getListeQuestionnaires()
                            .remove(getIndex());
                        getTableView().getItems().remove(questionnaire);
                        System.out.println(questionnaire.getTitre() + " supprime");

                      } catch (SQLException e1) {
                        e1.printStackTrace();
                      }
                    }
                  }
                });
              }

              @Override
              public void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                  setGraphic(null);
                } else {
                  setGraphic(headerBox);
                }
              }
            };
            return cell;
          }
        };

    colDelBtn.setCellFactory(cellFactory);

    questionnaireTableau.getColumns().add(colDelBtn);

  }

  /**
   * Cette fonction permet d'afficher un popup de confirmation pour la suppression d'un
   * questionnaire.
   *
   * @param questionnaire le questionnaire a supprimer.
   * @return true si l'utilisateur confirme la suppression, false sinon.
   */
  boolean confirmationSupprimerQuestionnaire(Questionnaire questionnaire) {
    Alert alert = new Alert(AlertType.CONFIRMATION);
    alert.initOwner(mainApp.getPrimaryStage());
    alert.initModality(Modality.WINDOW_MODAL);
    alert.setTitle("supprimer " + questionnaire.getTitre() + " ?");
    alert.setHeaderText("supprimer " + questionnaire.getTitre() + " ?");
    alert.setContentText("Etes-vous sur de vouloir supprimer ce questionnaire ?");

    Optional<ButtonType> result = alert.showAndWait();
    if (!result.isPresent() || result.get() != ButtonType.OK) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * Cette fonction est appele avec le Main pour donner sa reference.
   *
   * @param mainApp application Main.
   */
  public void setMainApp(App mainApp) {
    this.mainApp = mainApp;

    // Some observable lists data to the table
    questionnaireTableau.setItems(mainApp.getGestionSportifs().getQuestionnaireData());
  }

}
