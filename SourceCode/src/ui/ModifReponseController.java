package ui;

import application.questionnaire.Questionnaire;
import application.questionnaire.Reponses;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ModifReponseController {

  @FXML
  private Text mofif;

  @FXML
  private Label intitule;

  @FXML
  private ChoiceBox<String> reponseBox;

  private Reponses reponse;

  private int indexQ;

  @SuppressWarnings("unused")
  private App mainApp;

  private Stage modifReponseStage;
  private boolean okClicked;

  /**
   * Initialiser la classe controleur. Cette methode est automatiquement appele apres que le fichier
   * fxml ait ete charge. La fonction n'est pas utilis� mais obligatoire.
   */
  @FXML
  private void initialize() {}

  /**
   * Initialise la variable question et le TextArea intituleQuestion avec l'intitul� de la question.
   *
   * @param reponse reponse a modifier
   */
  public void setReponse(Reponses reponse, int indexQ, Questionnaire questionnaire) {

    this.reponse = reponse;
    this.indexQ = indexQ;
    System.out.println(reponse.getQuestionnaireReponses().getTitre());
    System.out.println(questionnaire.getTitre());
    this.intitule.setText(questionnaire.getListeQuestions().get(indexQ).getTexteQuestion());
    this.reponseBox.setItems(FXCollections.observableArrayList("Oui", "Non"));
    try {
      if (this.reponse.getListeReponses().get(0) == 0) {
        this.reponseBox.getSelectionModel().select(1);
      } else {
        this.reponseBox.getSelectionModel().select(0);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

  }



  /**
   * Est appel� quand l'utilisateur appuie sur le bouton OK La fonction initialise l'intitul� de la
   * question avec la valeur indiqu� dans le TextArea intituleQuestion.
   */
  @FXML
  private void handleOk() {
    if (isInputValid()) {
      if (this.reponseBox.getSelectionModel().getSelectedIndex() == 0) {
        this.reponse.modifieReponse(indexQ, 1);
      } else {
        this.reponse.modifieReponse(indexQ, 2);
      }

      this.okClicked = true;
      modifReponseStage.close();
    }
  }

  /**
   * verification des textField.
   *
   * @return true si les inputs sont valides
   */
  private boolean isInputValid() {
    String errorMessage = "";

    if (this.reponseBox.getSelectionModel().getSelectedItem() == null) {
      errorMessage += "reponse invalide !\n";
    }

    if (errorMessage.length() == 0) {
      return true;
    } else {
      // Show the error message.
      Alert alert = new Alert(AlertType.ERROR);
      alert.initOwner(modifReponseStage);
      alert.setTitle("champ(s) invalide(s)");
      alert.setHeaderText("veuillez rectifier vos informations");
      alert.setContentText(errorMessage);

      alert.showAndWait();

      return false;
    }
  }

  /**
   * Cette fonction est appele avec le Main pour donner sa reference.
   *
   * @param mainApp application Main.
   */
  public void setMainApp(App mainApp) {
    this.mainApp = mainApp;

  }

  /**
   * defini le stage de la fenetre.
   *
   * @param modifQuestionStage le stage a definir pour ce controller
   */
  public void setDialogStage(Stage modifQuestionStage) {
    this.modifReponseStage = modifQuestionStage;
  }

  /**
   * retourne true si l'utilisateur clique sur OK, sinon false.
   *
   * @return le boolean qui verifie si l'utilisateur a clique sur ok
   */
  public boolean isOkClicked() {
    return this.okClicked;
  }

}
