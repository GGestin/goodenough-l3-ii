package ui;

import application.sportif.Sportif;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcons;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.util.Callback;

public class ListeSportifController {

  @FXML
  private TableView<Sportif> sportifTableau;
  @FXML
  private TableColumn<Sportif, String> sportifs;

  // Reference a l'application Main.
  private App mainApp;

  /**
   * Le constructeur,appel� avant la methode initialiser().
   */
  public ListeSportifController() {}

  /**
   * Initialiser la classe controleur. Cette methode est automatiquement appele apres que le fichier
   * fxml ait ete charge.
   */
  @FXML
  private void initialize() {
    // Initialize the person table with the two columns.
    sportifs.setCellValueFactory(cellData -> cellData.getValue().afficheListe());
    this.ajouterBoutonModifierTableauSportif();
    // this.ajouterBoutonStatsTableauSportif();
    this.ajouterBoutonSupprimerTableauSportif();
  }

  /**
   * appeler quand l'utilisateur appuie sur le bouton "ajouter" d'un sportif. ca affiche un dialogue
   * pour rentrer les donnes du nouveau sportif et la possibilite de repondre aux questionnaires.
   */
  @FXML
  private void handleNewSportif() {
    Sportif ns = new Sportif();
    boolean okClicked = mainApp.showSportifOverview(ns);
    if (okClicked) {
      mainApp.getGestionSportifs().getSportifs().ajoutSportif(ns.getId(), ns.getNom(),
          ns.getPrenom(), ns.getPseudo(), ns.getMotDePasse(), ns.getDateDeNaissance(),
          ns.getActiviteSportive().name());
      mainApp.getGestionSportifs().getSportifData().add(ns);
    }
    this.sportifTableau.refresh();
  }

  /**
   * ajoute du bouton modifier au tableau des sportifs.
   */
  private void ajouterBoutonModifierTableauSportif() {
    TableColumn<Sportif, Void> colModBtn = new TableColumn<Sportif, Void>();
    colModBtn.setMaxWidth(500);

    Callback<TableColumn<Sportif, Void>, TableCell<Sportif, Void>> cellFactory =
        new Callback<TableColumn<Sportif, Void>, TableCell<Sportif, Void>>() {
          @Override
          public TableCell<Sportif, Void> call(final TableColumn<Sportif, Void> param) {
            final TableCell<Sportif, Void> cell = new TableCell<Sportif, Void>() {

              private final HBox headerBox = new HBox();

              {
                headerBox.getStyleClass().add("header_component");
                headerBox.getChildren()
                    .addAll(GlyphsDude.createIcon(FontAwesomeIcons.PENCIL, "2.5em"));
                headerBox.setOnMouseClicked(e -> {
                  if (e.getButton() == MouseButton.PRIMARY) {
                    Sportif sportif = getTableView().getItems().get(getIndex());
                    mainApp.showSportifOverview(sportif);
                    System.out.println("le sportif " + sportif.getPrenomNom() + " a ete modifie");
                    sportifTableau.refresh();
                  }
                });
              }

              @Override
              public void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                  setGraphic(null);
                } else {
                  setGraphic(headerBox);
                }
              }
            };
            return cell;
          }
        };

    colModBtn.setCellFactory(cellFactory);

    sportifTableau.getColumns().add(colModBtn);

  }

  /*
   * ajoute du bouton statistiques au tableau des sportifs.
   */
  /*
   * private void ajouterBoutonStatsTableauSportif() { TableColumn<Sportif, Void> colStatBtn = new
   * TableColumn<Sportif, Void>();
   *
   * Callback<TableColumn<Sportif, Void>, TableCell<Sportif, Void>> cellFactory = new
   * Callback<TableColumn<Sportif, Void>, TableCell<Sportif, Void>>() {
   *
   * @Override public TableCell<Sportif, Void> call(final TableColumn<Sportif, Void> param) { final
   * TableCell<Sportif, Void> cell = new TableCell<Sportif, Void>() {
   *
   * private final Button btn = new Button("Stats");
   *
   * { btn.setOnAction((ActionEvent event) -> { Sportif sportif =
   * getTableView().getItems().get(getIndex()); System.out.println("statistiques du sportif " +
   * sportif.getPrenomNom()); }); }
   *
   * @Override public void updateItem(Void item, boolean empty) { super.updateItem(item, empty); if
   * (empty) { setGraphic(null); } else { setGraphic(btn); } } }; return cell; } };
   *
   * colStatBtn.setCellFactory(cellFactory);
   *
   * sportifTableau.getColumns().add(colStatBtn);
   *
   * }
   */

  /**
   * ajoute du bouton supprimer au tableau des sportifs.
   */
  private void ajouterBoutonSupprimerTableauSportif() {
    TableColumn<Sportif, Void> colDelBtn = new TableColumn<Sportif, Void>();
    colDelBtn.setMaxWidth(500);

    Callback<TableColumn<Sportif, Void>, TableCell<Sportif, Void>> cellFactory =
        new Callback<TableColumn<Sportif, Void>, TableCell<Sportif, Void>>() {
          @Override
          public TableCell<Sportif, Void> call(final TableColumn<Sportif, Void> param) {
            final TableCell<Sportif, Void> cell = new TableCell<Sportif, Void>() {

              private final HBox headerBox = new HBox();

              {
                headerBox.getStyleClass().add("header_component");
                headerBox.getChildren()
                    .addAll(GlyphsDude.createIcon(FontAwesomeIcons.TRASH, "2.5em"));
                headerBox.setOnMouseClicked(e -> {
                  if (e.getButton() == MouseButton.PRIMARY) {
                    Sportif sportif = getTableView().getItems().get(getIndex());
                    System.out.println(sportif.getId());

                    if (confirmationSupprimerSportif(sportif)) {
                      // suppression du sportif avec connexion a la base de donnees
                      Connection con = mainApp.getGestionSportifs().connexionBaseDeDonnees();
                      // requete pour recuperer toutes les listes de reponses du questionnaire
                      String requete = "SELECT idReponses FROM reponses WHERE sportifReponses = ?";
                      ResultSet resultats = null;

                      try {
                        PreparedStatement stmt = con.prepareStatement(requete);
                        stmt.setInt(1, sportif.getId());
                        resultats = stmt.executeQuery();
                      } catch (SQLException exception) {
                        System.err.println("Anomalie lors de l'execution de la requ�te "
                            + "SELECT idReponses FROM question WHERE questionnaireReponses = ?");
                      }

                      // boucle sur chaque liste de reponses du questionnaire
                      try {
                        boolean encore = resultats.next();

                        while (encore) {
                          ResultSet res = null;

                          // requete pour recuperer chaque reponse de la liste de reponses
                          String req = "SELECT idReponse FROM reponse WHERE lesReponses = ?";
                          try {
                            PreparedStatement stmt = con.prepareStatement(req);
                            stmt.setInt(1, resultats.getInt(1));
                            res = stmt.executeQuery();
                          } catch (SQLException exception) {
                            System.err.println("Anomalie lors de l'execution de la requ�te "
                                + "SELECT idReponse FROM reponse WHERE laQuestion = ?");
                          }

                          // boucle sur chaque reponse de la liste de reponses
                          try {
                            boolean again = res.next();

                            while (again) {

                              // requete de suppression de chaque reponse de la liste de reponses
                              String requeteDeleteReponse =
                                  "DELETE FROM reponse WHERE idReponse = ?";

                              try {
                                PreparedStatement stmt = con.prepareStatement(requeteDeleteReponse);
                                stmt.setInt(1, res.getInt(1));
                                stmt.execute();
                                mainApp.getGestionSportifs().getReponses().getListeReponses()
                                    .get(resultats.getInt(1)).getListeReponses()
                                    .remove(res.getInt(1));


                              } catch (SQLException e1) {
                                e1.printStackTrace();
                              }

                              again = res.next();
                            }
                          } catch (SQLException exception) {
                            System.err.println(exception.getMessage());
                          }

                          // requete de suppression de chaque liste de reponses du questionnaire
                          String requeteDeleteLR = "DELETE FROM reponses WHERE idReponses = ?";

                          try {
                            PreparedStatement stmt = con.prepareStatement(requeteDeleteLR);
                            stmt.setInt(1, resultats.getInt(1));
                            stmt.execute();
                            mainApp.getGestionSportifs().getReponsesData()
                                .remove(resultats.getInt(1));
                            mainApp.getGestionSportifs().getReponses().getListeReponses()
                                .remove(resultats.getInt(1));

                          } catch (SQLException e1) {
                            e1.printStackTrace();
                          }

                          encore = resultats.next();
                        }

                        resultats.close();
                      } catch (SQLException exception) {
                        System.err.println(exception.getMessage());
                      }


                      // requete de suppression du questionnaire
                      requete = "DELETE FROM sportif WHERE idSportif = ?";

                      try {
                        PreparedStatement stmt = con.prepareStatement(requete);
                        stmt.setInt(1, sportif.getId());
                        stmt.execute();
                        mainApp.getGestionSportifs().getSportifs().getListeSportifs()
                            .remove(getIndex());
                        mainApp.getGestionSportifs().getSportifData().remove(getIndex());
                        getTableView().getItems().remove(sportif);
                        System.out.println(sportif.getPrenomNom() + " supprime");
                        sportifTableau.refresh();

                      } catch (SQLException e1) {
                        e1.printStackTrace();
                      }
                    }
                  }
                });
              }

              @Override
              public void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                  setGraphic(null);
                } else {
                  setGraphic(headerBox);
                }
              }
            };
            return cell;
          }
        };

    colDelBtn.setCellFactory(cellFactory);

    sportifTableau.getColumns().add(colDelBtn);

  }

  /**
   * Cette fonction permet d'afficher un popup de confirmation pour la suppression d'un sportif.
   *
   * @param sportif le sportif a supprimer.
   * @return true si l'utilisateur confirme la suppression, false sinon.
   */
  boolean confirmationSupprimerSportif(Sportif sportif) {
    Alert alert = new Alert(AlertType.CONFIRMATION);
    alert.initOwner(mainApp.getPrimaryStage());
    alert.initModality(Modality.WINDOW_MODAL);
    alert.setTitle("supprimer " + sportif.getPrenomNom() + " ?");
    alert.setHeaderText("supprimer " + sportif.getPrenomNom() + " ?");
    alert.setContentText("Etes-vous sur de vouloir supprimer ce sportif ?");

    Optional<ButtonType> result = alert.showAndWait();
    if (!result.isPresent() || result.get() != ButtonType.OK) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * Cette fonction est appele avec le Main pour donner sa reference.
   *
   * @param mainApp application Main.
   */
  public void setMainApp(App mainApp) {
    this.mainApp = mainApp;

    // Some observable lists data to the table
    sportifTableau.setItems(mainApp.getGestionSportifs().getSportifData());
  }

}
