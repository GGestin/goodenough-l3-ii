package ui;

import application.questionnaire.Questionnaire;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class AjoutQuestionnaireController {

  @FXML
  private Text mofif;

  @FXML
  private Label intitule;

  @FXML
  private TextField titreQuestionnaire;

  private Questionnaire questionnaire;

  private App mainApp;

  private Stage ajoutQuestionnaireStage;
  private boolean okClicked;


  /**
   * Initialiser la classe controleur. Cette methode est automatiquement appele apres que le fichier
   * fxml ait ete charge. La fonction n'est pas utilis� mais obligatoire.
   */
  @FXML
  private void initialize() {}

  /**
   * Initialise la variable questionnaire.
   *
   * @param questionnaire le questionnaire � initialiser
   */
  public void setQuestionnaire(Questionnaire questionnaire) {
    this.questionnaire = questionnaire;
  }

  /**
   * Est appel� quand l'utilisateur appuie sur le bouton OK La fonction initialise le titre du
   * nouveau Questionnaire avec la valeur indiqu� dans le TextField TitreQuestionnaire.
   */
  @FXML
  private void handleOk() {
    if (this.isInputValid()) {

      // changement en base de donnees
      Connection con = mainApp.getGestionSportifs().connexionBaseDeDonnees();
      String requete = "INSERT INTO questionnaire (titreQuestionnaire, estOuvert) VALUES (?, ?)";

      try {
        PreparedStatement stmt = con.prepareStatement(requete);
        stmt.setString(1, this.titreQuestionnaire.getText());
        stmt.setBoolean(2, true);
        stmt.execute();

        System.out.println("test : " + this.titreQuestionnaire.getText());
        this.questionnaire.setTitreQuestionnaireProperty(this.titreQuestionnaire.textProperty());

        String req = "SELECT idQuestionnaire FROM questionnaire WHERE titreQuestionnaire = ?";
        ResultSet resultats = null;

        try {
          PreparedStatement pstmt = con.prepareStatement(req);
          pstmt.setString(1, this.titreQuestionnaire.getText());
          resultats = pstmt.executeQuery();
        } catch (SQLException exception) {
          System.err.println("Anomalie lors de l'execution de la requ�te "
              + "SELECT idQuestionnaire FROM questionnaire WHERE titreQuestionnaire = ?");
        }

        resultats.next();
        this.questionnaire.setId(resultats.getInt(1));

        resultats.close();

        okClicked = true;

      } catch (SQLException e) {
        e.printStackTrace();
      }

      ajoutQuestionnaireStage.close();
    }
  }

  /**
   * verification des textField.
   *
   * @return true si les inputs sont valides
   */
  private boolean isInputValid() {
    String errorMessage = "";

    if (this.titreQuestionnaire.textProperty().get() == null
        || this.titreQuestionnaire.textProperty().get().length() == 0) {
      errorMessage += "nom du questionnaire invalide !\n";
    }

    if (errorMessage.length() == 0) {
      return true;
    } else {
      // Show the error message.
      Alert alert = new Alert(AlertType.ERROR);
      alert.initOwner(ajoutQuestionnaireStage);
      alert.setTitle("champ(s) invalide(s)");
      alert.setHeaderText("veuillez rectifier vos informations");
      alert.setContentText(errorMessage);

      alert.showAndWait();

      return false;
    }
  }

  /**
   * Cette fonction est appele avec le Main pour donner sa reference.
   *
   * @param mainApp application Main.
   */
  public void setMainApp(App mainApp) {
    this.mainApp = mainApp;

  }

  /**
   * defini le stage de la fenetre.
   *
   * @param ajoutQuestionnaireStage c'est le stage a definir pour ce controller
   */
  public void setDialogStage(Stage ajoutQuestionnaireStage) {
    this.ajoutQuestionnaireStage = ajoutQuestionnaireStage;
  }

  /**
   * retourne true si l'utilisateur clique sur OK, sinon false.
   *
   * @return le boolean qui verifie si l'utilisateur a clique sur ok
   */
  public boolean isOkClicked() {
    return this.okClicked;
  }

}
