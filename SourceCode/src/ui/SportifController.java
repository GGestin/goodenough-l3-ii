package ui;

import application.sportif.SportType;
import application.sportif.Sportif;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class SportifController {

  @FXML
  private Label titre;
  @FXML
  private TextField nom;
  @FXML
  private TextField prenom;
  @FXML
  private TextField pseudo;
  @FXML
  private TextField motDePasse;
  @FXML
  private DatePicker dateDeNaissance;
  @FXML
  private ChoiceBox<SportType> activiteSportive;

  private Stage sportifStage;
  private Sportif sportif;
  private boolean okClicked;

  // Reference a l'application Main.
  private App mainApp;

  /**
   * Le constructeur,appel� avant la methode initialiser().
   */
  public SportifController() {}

  /**
   * Initialiser la classe controleur. Cette methode est automatiquement appele apres que le fichier
   * fxml ait ete charge.
   */
  @FXML
  private void initialize() {
    addActivitesSportives();
  }

  private void addActivitesSportives() {
    this.activiteSportive.getItems().setAll(SportType.values());
  }

  /**
   * defini le stage du la fenetre.
   *
   * @param sportifStage c'est le stage a definir pour ce controller
   */
  public void setDialogStage(Stage sportifStage) {
    this.sportifStage = sportifStage;
  }

  /**
   * met le sportif a editer dans la fenetre.
   *
   * @param sportif a ajouter
   */
  public void setSportif(Sportif sportif) {

    this.sportif = sportif;

    if (!(sportif.getNomProperty() == null)) {
      this.nom.setText(sportif.getNom());
      this.titre.setText("Modifier un sportif");
    } else {
      this.titre.setText("Ajouter un sportif");
    }

    if (!(sportif.getPrenomProperty() == null)) {
      this.prenom.setText(sportif.getPrenom());
    }

    if (!(sportif.getPseudoProperty() == null)) {
      this.pseudo.setText(sportif.getPseudo());
    }

    if (!(sportif.getActiviteSportive() == null)) {
      this.activiteSportive.setValue(sportif.getActiviteSportive());
    }

    if (!(sportif.getDateDeNaissance() == null)) {
      this.dateDeNaissance.setValue(sportif.getDateDeNaissance());
    }
  }

  /**
   * retourne true si l'utilisateur clique sur OK, sinon false.
   *
   * @return le boolean qui verifie si l'utilisateur a clique sur ok
   */
  public boolean isOkClicked() {
    return okClicked;
  }

  /**
   * appele quand l'utilisateur clique sur ok.
   */
  @FXML
  private void handleOk() {
    if (isInputValid()) {

      // changement en base de donnees
      Connection con = mainApp.getGestionSportifs().connexionBaseDeDonnees();
      String requete;

      if (this.sportif.getNom() == null) {
        requete = "INSERT INTO sportif (nomSportif, prenomSportif, pseudoSportif,"
            + "motDePasseSportif, dateDeNaissanceSportif, activiteSportive)"
            + " VALUES (?, ?, ?, ?, ?, ?)";

        try {
          PreparedStatement stmt = con.prepareStatement(requete);
          stmt.setString(1, this.nom.getText());
          stmt.setString(2, this.prenom.getText());
          stmt.setString(3, this.pseudo.getText());
          stmt.setString(4, hasher(this.motDePasse.getText()));
          stmt.setDate(5, Date.valueOf(this.dateDeNaissance.getValue()));
          stmt.setString(6, this.activiteSportive.getValue().name());
          stmt.execute();

          this.sportif.setNomProperty(this.nom.textProperty());
          this.sportif.setPrenomProperty(this.prenom.textProperty());
          this.sportif.setPseudoProperty(this.pseudo.textProperty());
          this.sportif
              .setMotDePasseProperty(new SimpleStringProperty(hasher(this.motDePasse.getText())));
          this.sportif.setDateDeNaissance(this.dateDeNaissance.getValue());
          this.sportif.setActiviteSportive(this.activiteSportive.getValue());

          String req = "SELECT idSportif FROM sportif WHERE pseudoSportif = ?";
          ResultSet resultats = null;

          try {
            PreparedStatement pstmt = con.prepareStatement(req);
            pstmt.setString(1, this.pseudo.getText());
            resultats = pstmt.executeQuery();
          } catch (SQLException exception) {
            System.err.println("Anomalie lors de l'execution de la requ�te "
                + "SELECT idSportif FROM sportif WHERE pseudoSportif = ?");
          }

          resultats.next();
          this.sportif.setId(resultats.getInt(1));

          resultats.close();


          okClicked = true;

        } catch (SQLException e) {
          e.printStackTrace();
        }
      } else {
        if (this.motDePasse.getText() != null && this.motDePasse.getText().length() > 0) {
          requete = "UPDATE sportif SET nomSportif = ?, prenomSportif = ?, pseudoSportif = ?,"
              + "motDePasseSportif = ?,  dateDeNaissanceSportif = ?, activiteSportive = ? "
              + "WHERE idSportif = ?";
        } else {
          System.out.println("ici");
          requete = "UPDATE sportif SET nomSportif = ?, prenomSportif = ?, pseudoSportif = ?,"
              + " dateDeNaissanceSportif = ?, activiteSportive = ? WHERE idSportif = ?";

        }

        try {
          PreparedStatement stmt = con.prepareStatement(requete);
          stmt.setString(1, this.nom.getText());
          stmt.setString(2, this.prenom.getText());
          stmt.setString(3, this.pseudo.getText());
          if (this.motDePasse.getText() != null && this.motDePasse.getText().length() > 0) {
            stmt.setString(4, hasher(this.motDePasse.getText()));
            stmt.setDate(5, Date.valueOf(this.dateDeNaissance.getValue()));
            stmt.setString(6, this.activiteSportive.getValue().name());
            stmt.setInt(7, this.sportif.getId());
          } else {
            stmt.setDate(4, Date.valueOf(this.dateDeNaissance.getValue()));
            stmt.setString(5, this.activiteSportive.getValue().name());
            stmt.setInt(6, this.sportif.getId());
          }
          stmt.execute();

          this.sportif.setNomProperty(this.nom.textProperty());
          this.sportif.setPrenomProperty(this.prenom.textProperty());
          this.sportif.setPseudoProperty(this.pseudo.textProperty());
          if (this.motDePasse.getText() != null && this.motDePasse.getText().length() > 0) {
            this.sportif
                .setMotDePasseProperty(new SimpleStringProperty(hasher(this.motDePasse.getText())));
          }
          this.sportif.setDateDeNaissance(this.dateDeNaissance.getValue());
          this.sportif.setActiviteSportive(this.activiteSportive.getValue());

          okClicked = true;

        } catch (SQLException e) {
          e.printStackTrace();
        }
      }


      sportifStage.close();

      if (!(this.motDePasse.getText() == null || this.motDePasse.getText().length() == 0)) {
        this.sportif.setMotDePasseProperty(this.motDePasse.textProperty());
      }
    }
  }

  /**
   * appele quand l'utilisateur clique sur annuler.
   */
  @FXML
  private void handleCancel() {
    sportifStage.close();
  }

  /**
   * verification des textField.
   *
   * @return true si les inputs sont valides
   */
  private boolean isInputValid() {
    String errorMessage = "";

    if (this.nom.getText() == null || this.nom.getText().length() == 0) {
      errorMessage += "nom invalide !\n";
    }
    if (this.prenom.getText() == null || this.prenom.getText().length() == 0) {
      errorMessage += "prenom invalide !\n";
    }
    if (this.pseudo.getText() == null || this.pseudo.getText().length() == 0) {
      errorMessage += "pseudo invalide !\n";
    }
    if ((this.motDePasse.getText() == null || this.motDePasse.getText().length() == 0)
        && (this.sportif.getMotDePasse() == null || this.sportif.getMotDePasse().length() == 0)) {
      errorMessage += "mot de passe invalide en mode creation !\n";
    }

    if (this.dateDeNaissance.getValue() == null) {
      errorMessage += "date de naissance invalide !\n";
    }

    if (errorMessage.length() == 0) {
      return true;
    } else {
      // Show the error message.
      Alert alert = new Alert(AlertType.ERROR);
      alert.initOwner(sportifStage);
      alert.setTitle("champ(s) invalide(s)");
      alert.setHeaderText("veuillez rectifier vos informations");
      alert.setContentText(errorMessage);

      alert.showAndWait();

      return false;
    }
  }

  /**
   * Cette fonction est appele avec le Main pour donner sa reference.
   *
   * @param mainApp application Main.
   */
  public void setMainApp(App mainApp) {
    this.mainApp = mainApp;
  }

  /**
   * fonction de hashage du mot de passe.
   *
   * @param mdp est le mot de passe a hasher
   * @return le mot de passe hashe
   */

  public String hasher(String mdp) {
    MessageDigest md;
    try {
      md = MessageDigest.getInstance("MD5");
      byte[] hashInBytes = md.digest(mdp.getBytes(StandardCharsets.UTF_8));
      StringBuilder sb = new StringBuilder();
      for (byte b : hashInBytes) {
        sb.append(String.format("%02x", b));
      }
      return sb.toString();
    } catch (NoSuchAlgorithmException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return null;

  }

}
