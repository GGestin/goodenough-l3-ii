package ui;

import application.questionnaire.Question;
import application.questionnaire.Questionnaire;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcons;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.util.Callback;

public class QuestionnaireController {

  @FXML
  private TableView<Question> questionTable;
  @FXML
  private TableColumn<Question, String> intituleQuestion;

  @FXML
  private TextField labelQuestionnaire;

  @FXML
  private Button buttonAjouter;

  private App mainApp;

  private int indexQuestionnaire;

  private ObservableList<Question> questionData = FXCollections.observableArrayList();

  /**
   * Le constructeur, appel� avant la fonction initialiser.
   */
  public QuestionnaireController() {}

  /**
   * Initialiser la classe controleur. Cette methode est automatiquement appele apres que le fichier
   * fxml ait ete charge. La fonction n'est pas utilis�
   */
  @FXML
  private void initialize() {}

  /**
   * getter de la variable indexQuestionnaire.
   *
   * @return la variable indexQuestionnaire
   */
  public int getIndexQuestionnaire() {
    return indexQuestionnaire;
  }

  /**
   * Permet d'initialiser la vue en r�cup�rant l'index du Questionnaire voulu et en pla�ant ses
   * questions dans le tableau.
   *
   * @param indexQuestionnaire l'index du questionnaire
   */
  public void setIndexQuestionnaire(int indexQuestionnaire) {

    this.indexQuestionnaire = indexQuestionnaire;
    System.out.println("id : " + this.indexQuestionnaire);
    for (Questionnaire q : mainApp.getGestionSportifs().getQuestionnaires()
        .getListeQuestionnaires()) {
      if (q.getId() == this.indexQuestionnaire) {
        labelQuestionnaire.setText(q.getTitre());
        if (q.getListeQuestions() != null) {
          questionData.addAll(q.getListeQuestions());
          questionTable.setItems(questionData);
        }
      }
    }
    /*
     * questionData.addAll(mainApp.getQuestionnaireData()
     * .get(indexQuestionnaire).getListeQuestions());
     */
    intituleQuestion.setCellValueFactory(cellData -> cellData.getValue().getTexteProperty());
    // questionTable.setItems(questionData);

    // this.ajouterChoiceBox();
    this.ajouterModifier();
    this.ajouterBoutonSuppr();
  }

  /**
   * Est appel� quand l'utilisateur appuie sur le bouton Ajouter Question.
   */
  @FXML
  private void handleNewQuestion() {
    Question nouvelleQuestion = new Question(0, null, 0);
    boolean okClicked =
        mainApp.showModifQuestionOverview(nouvelleQuestion, this.indexQuestionnaire);
    if (okClicked) {
      for (Questionnaire q : mainApp.getGestionSportifs().getQuestionnaires()
          .getListeQuestionnaires()) {
        if (q.getId() == this.indexQuestionnaire) {
          q.ajoutQuestion(nouvelleQuestion.getId(), nouvelleQuestion.getTexteQuestion(),
              nouvelleQuestion.getReponseDefaut());
          this.questionData.add(nouvelleQuestion);
        }
      }
      // this.questionTable.refresh();
    }
  }

  @FXML
  private void handleBoutonValider() {
    for (Questionnaire q : mainApp.getGestionSportifs().getQuestionnaires()
        .getListeQuestionnaires()) {
      if (q.getId() == this.indexQuestionnaire) {
        q.setTitreQuestionnaire(this.labelQuestionnaire.getText());
      }
    }

    mainApp.showQuestionnaireOverview(this.indexQuestionnaire);
  }

  /**
   * ajoute le bouton modifier � chaque colonne du tableview.
   */
  private void ajouterModifier() {
    TableColumn<Question, Void> columnModif = new TableColumn<Question, Void>();
    columnModif.setMaxWidth(250);

    Callback<TableColumn<Question, Void>, TableCell<Question, Void>> cellFactory =
        new Callback<TableColumn<Question, Void>, TableCell<Question, Void>>() {

          @Override
          public TableCell<Question, Void> call(TableColumn<Question, Void> param) {
            final TableCell<Question, Void> cell = new TableCell<Question, Void>() {

              private final HBox headerBox = new HBox();

              {
                headerBox.getStyleClass().add("header_component");
                headerBox.getChildren()
                    .addAll(GlyphsDude.createIcon(FontAwesomeIcons.PENCIL, "2.5em"));
                headerBox.setOnMouseClicked(e -> {
                  if (e.getButton() == MouseButton.PRIMARY) {
                    Question question = getTableView().getItems().get(getIndex());
                    System.out.println(question.getId());
                    mainApp.showModifQuestionOverview(question, ((indexQuestionnaire * 10) + 1));
                    questionTable.refresh();
                  }
                });
              }

              @Override
              public void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                  setGraphic(null);
                } else {
                  setGraphic(headerBox);
                }
              }
            };
            return cell;
          }
        };
    columnModif.setCellFactory(cellFactory);
    questionTable.getColumns().add(columnModif);
  }

  /*
   * /** ajoute une ChoiceBox � chaque colonne du tableview.
   */
  /*
   * private void ajouterChoiceBox() { TableColumn<Question, Void> columnChoix = new
   * TableColumn<Question, Void>(); columnChoix.setMaxWidth(750);
   *
   * Callback<TableColumn<Question, Void>, TableCell<Question, Void>> cellFactory = new
   * Callback<TableColumn<Question, Void>, TableCell<Question, Void>>() {
   *
   * @Override public TableCell<Question, Void> call(TableColumn<Question, Void> param) { final
   * TableCell<Question, Void> cell = new TableCell<Question, Void>() { private final
   * ChoiceBox<String> choiceb = new ChoiceBox<String>(FXCollections.observableArrayList("Non",
   * "Oui"));
   *
   * { for (int i = 0; i < mainApp.getGestionSportifs().getQuestionnaires()
   * .getListeQuestionnaires().get(indexQuestionnaire).getListeQuestions() .size(); i++) { //
   * choiceb.getSelectionModel.select(1); } choiceb.getSelectionModel().select(0);
   * choiceb.setOnAction((ActionEvent event) -> { if (choiceb.getSelectionModel().getSelectedIndex()
   * != mainApp.getGestionSportifs()
   * .getQuestionnaires().getListeQuestionnaires().get(getIndexQuestionnaire())
   * .getListeQuestions().get(getIndex()).getReponseDefaut()) { if
   * (choiceb.getSelectionModel().getSelectedIndex() == 0) {
   * mainApp.getGestionSportifs().getQuestionnaires().getListeQuestionnaires()
   * .get(getIndexQuestionnaire()).getListeQuestions().get(getIndex()) .setReponseDefaut(0); } else
   * { mainApp.getGestionSportifs().getQuestionnaires().getListeQuestionnaires()
   * .get(getIndexQuestionnaire()).getListeQuestions().get(getIndex()) .setReponseDefaut(1); } } });
   * }
   *
   * @Override public void updateItem(Void item, boolean empty) { super.updateItem(item, empty); if
   * (empty) { setGraphic(null); } else { setGraphic(choiceb); } } }; return cell; } };
   * columnChoix.setCellFactory(cellFactory); questionTable.getColumns().add(columnChoix); }
   */

  /**
   * ajoute un bouton supprimer � chaque colonne du tableview.
   */
  private void ajouterBoutonSuppr() {

    TableColumn<Question, Void> columnSupprQuestion = new TableColumn<Question, Void>();
    columnSupprQuestion.setMaxWidth(250);

    Callback<TableColumn<Question, Void>, TableCell<Question, Void>> cellFactory =
        new Callback<TableColumn<Question, Void>, TableCell<Question, Void>>() {

          @Override
          public TableCell<Question, Void> call(TableColumn<Question, Void> param) {
            final TableCell<Question, Void> cell = new TableCell<Question, Void>() {

              private final HBox headerBox = new HBox();

              {
                headerBox.getStyleClass().add("header_component");
                headerBox.getChildren()
                    .addAll(GlyphsDude.createIcon(FontAwesomeIcons.TRASH, "2.5em"));
                headerBox.setOnMouseClicked(e -> {
                  if (e.getButton() == MouseButton.PRIMARY) {
                    Question question = getTableView().getItems().get(getIndex());

                    if (confirmationSupprimerQuestion(question)) {
                      // connexion a la base de donnees
                      Connection con = mainApp.getGestionSportifs().connexionBaseDeDonnees();

                      // requete de suppression de chaque reponse de la question
                      String requeteDeleteReponse = "DELETE FROM reponse WHERE laQuestion = ?";

                      try {
                        PreparedStatement stmt = con.prepareStatement(requeteDeleteReponse);
                        stmt.setInt(1, question.getId());
                        stmt.execute();

                      } catch (SQLException e1) {
                        e1.printStackTrace();
                      }

                      // requete de suppression de chaque question du questionnaire
                      String requeteDeleteQuestion = "DELETE FROM question WHERE idQuestion = ?";

                      try {
                        PreparedStatement stmt = con.prepareStatement(requeteDeleteQuestion);
                        stmt.setInt(1, question.getId());
                        stmt.execute();
                        for (Questionnaire q : mainApp.getGestionSportifs().getQuestionnaires()
                            .getListeQuestionnaires()) {
                          if (q.getId() == getIndexQuestionnaire()) {
                            q.getListeQuestions().remove(getIndex());
                          }
                        }
                        getTableView().getItems().remove(question);

                      } catch (SQLException e1) {
                        e1.printStackTrace();
                      }
                    }
                  }
                });
              }

              @Override
              public void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                  setGraphic(null);
                } else {
                  setGraphic(headerBox);
                }
              }
            };
            return cell;
          }
        };
    columnSupprQuestion.setCellFactory(cellFactory);
    questionTable.getColumns().add(columnSupprQuestion);
  }

  /**
   * Cette fonction permet d'afficher un popup de confirmation pour la suppression d'une question.
   *
   * @param question la question a supprimer.
   * @return true si l'utilisateur confirme la suppression, false sinon.
   */
  boolean confirmationSupprimerQuestion(Question question) {
    Alert alert = new Alert(AlertType.CONFIRMATION);
    alert.initOwner(mainApp.getPrimaryStage());
    alert.initModality(Modality.WINDOW_MODAL);
    alert.setTitle("supprimer " + question.getTexteQuestion() + " ?");
    alert.setHeaderText("supprimer " + question.getTexteQuestion() + " ?");
    alert.setContentText("Etes-vous sur de vouloir supprimer cette question ?");

    Optional<ButtonType> result = alert.showAndWait();
    if (!result.isPresent() || result.get() != ButtonType.OK) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * Cette fonction est appele avec le Main pour donner sa reference.
   *
   * @param mainApp application Main.
   */
  public void setMainApp(App mainApp) {
    this.mainApp = mainApp;

    // Some observable lists data to the table
    // questionTable.setItems(questionData);
  }
}
