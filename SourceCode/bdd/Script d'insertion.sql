-- AJOUT SPORTIF --

INSERT INTO `heroku_915839e765a87e5`.`sportif`
(
`nomSportif`,
`prenomSportif`,
`pseudoSportif`,
`motDePasseSportif`,
`dateDeNaissanceSportif`,
`activiteSportive`)
VALUES
(
'FRED',
'David',
'GoodEnough',
'5a105e8b9d40e1329780d62ea2265d8a',
DATE('2000-10-1'),
'NATATION');

INSERT INTO `heroku_915839e765a87e5`.`sportif`
(
`nomSportif`,
`prenomSportif`,
`pseudoSportif`,
`motDePasseSportif`,
`dateDeNaissanceSportif`,
`activiteSportive`)
VALUES
(
'JOESTAR',
'Jotaro',
'TheDestroyerP29',
'ad0234829205b9033196ba818f7a872b',
DATE('1998-8-14'),
'BADMINTON');

INSERT INTO `heroku_915839e765a87e5`.`sportif`
(
`nomSportif`,
`prenomSportif`,
`pseudoSportif`,
`motDePasseSportif`,
`dateDeNaissanceSportif`,
`activiteSportive`)
VALUES
(
'test',
'test',
'test',
'8ad8757baa8564dc136c1e07507f4a98',
DATE('2011-11-11'),
'SKI');

-- AJOUT QUESTIONNAIRE --

INSERT INTO `heroku_915839e765a87e5`.`questionnaire`
(
`titreQuestionnaire`,
`estOuvert`)
VALUES
(
'Questionnaire de Santé',
1);

INSERT INTO `heroku_915839e765a87e5`.`questionnaire`
(
`titreQuestionnaire`,
`estOuvert`)
VALUES
(
'L empire contre attaque',
1);

-- AJOUT REPONSES --

INSERT INTO `heroku_915839e765a87e5`.`reponses`
(
`dateReponses`,
`sportifReponses`,
`questionnaireReponses`)
VALUES
(
NOW(),
1,
1);

INSERT INTO `heroku_915839e765a87e5`.`reponses`
(
`dateReponses`,
`sportifReponses`,
`questionnaireReponses`)
VALUES
(
NOW(),
11,
11);

-- AJOUT QUESTION --

INSERT INTO `heroku_915839e765a87e5`.`question`
(
`textQuestion`,
`reponseDefaut`,
`leQuestionnaire`)
VALUES
(
'Avez vous le COVID 19 ?',
0,
1);

INSERT INTO `heroku_915839e765a87e5`.`question`
(
`textQuestion`,
`reponseDefaut`,
`leQuestionnaire`)
VALUES
(
'Etes vous fatigué ?',
1,
1);

INSERT INTO `heroku_915839e765a87e5`.`question`
(
`textQuestion`,
`reponseDefaut`,
`leQuestionnaire`)
VALUES
(
'Jedi ou Sith ?',
0,
11);

INSERT INTO `heroku_915839e765a87e5`.`question`
(
`textQuestion`,
`reponseDefaut`,
`leQuestionnaire`)
VALUES
(
'Les derniers films meilleurs que les premiers ?',
1,
11);

INSERT INTO `heroku_915839e765a87e5`.`question`
(
`textQuestion`,
`reponseDefaut`,
`leQuestionnaire`)
VALUES
(
'JarJar beans méchant ?',
1,
11);

-- AJOUT REPONSE --

INSERT INTO `heroku_915839e765a87e5`.`reponse`
(`reponse`,
`lesReponses`,
`laQuestion`)
VALUES
(0,
1,
1);

INSERT INTO `heroku_915839e765a87e5`.`reponse`
(`reponse`,
`lesReponses`,
`laQuestion`)
VALUES
(1,
1,
11);

INSERT INTO `heroku_915839e765a87e5`.`reponse`
(`reponse`,
`lesReponses`,
`laQuestion`)
VALUES
(1,
11,
21);

INSERT INTO `heroku_915839e765a87e5`.`reponse`
(`reponse`,
`lesReponses`,
`laQuestion`)
VALUES
(0,
11,
31);

INSERT INTO `heroku_915839e765a87e5`.`reponse`
(`reponse`,
`lesReponses`,
`laQuestion`)
VALUES
(0,
11,
41);