const mysql = require('mysql');
const express = require('express');
const app = express();
const cors = require('cors');
const router = express.Router();
require('dotenv').config();

const PORT = process.env.PORT || 5000;

try {
  var sql_pool = mysql.createPool({
    connectionLimit: 5,
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE
  });
  console.log('SQL Connection Pool OK');
} catch (error) {
  console.error('SQL Connection Pool ERROR = ' + error);
}

router.use((req, res, next) => {
  var d = new Date();
  var n = d.toLocaleString();
  console.log('Time:', n);
  var ip = req.headers['x-forwarded-for'];
  console.log("Incoming: ", ip);
  next();
});

router.get('/login', (req, res) => {
  res.set('Access-Control-Allow-Origin', '*');
  const { id, ps } = req.query;
  var tables = ['Sportif'];
  var query = sql_pool.query('SELECT * FROM ?? WHERE pseudoSportif = ? AND motDePasseSportif = ?;', [tables, id, ps], (err, resQuery) => {
    console.log(query.sql);
    console.log("Fetched: ", resQuery.length);
    // Case Error
    if (err) {
        console.log("Error: ", err);
        res.sendStatus(err);
    }
    // Case Ok
    if (resQuery.length) {
      console.log("-- Fin de la requete login");
      res.sendStatus(200);
    } else {
      console.log("--ERROR Fin de la requete login");
      res.sendStatus(404);
    }
  });
});

router.get('/get', (req, res) => {
  res.set('Access-Control-Allow-Origin', '*');
  const { id } = req.query;
  var columns = ['idSportif','nomSportif', 'prenomSportif', 'pseudoSportif', 'dateDeNaissanceSportif', 'activiteSportive'];
  var tables = ['Sportif'];
  var query = sql_pool.query('SELECT ?? FROM ?? WHERE pseudoSportif = ?', [columns, tables, id], (err, resQuery) => {
    console.log(query.sql);
    console.log("Fetched: ", resQuery.length);
    // Case Error
    if (err) {
      console.log("Error: ", err);
      res.status(500).send(err);
    }
    // Case Ok
    console.log("-- Fin de la requete get");
    res.status(200).send(resQuery);
  });
});

router.get('/getQuestionnaire', (req, res) => {
  res.set('Access-Control-Allow-Origin', '*');
  var columns = ['idQuestion', 'textQuestion', 'reponseDefaut'];
  var tables = ['Question'];
  const { idQuestionnaire } = req.query;
  if (!idQuestionnaire) {
    return;
  }
  var query = sql_pool.query('SELECT ?? FROM ?? WHERE leQuestionnaire = ?', [columns, tables, idQuestionnaire], (err, resQuery) => {
    console.log(query.sql);
    console.log("Fetched: ", resQuery.length);
    // Case Error
    if (err) {
      console.log("Error: ", err);
      res.status(500).send(err);
    }
    // Case Ok
    console.log("-- Fin de la requete get Questionnaire");
    res.status(200).send(resQuery);
  });
});

router.get('/getQuestionnaires', (req, res) => {
  res.set('Access-Control-Allow-Origin', '*');
  var columns = ['idQuestionnaire', 'titreQuestionnaire'];
  var tables = ['Questionnaire'];
  var query = sql_pool.query('SELECT ?? FROM ?? WHERE estOuvert = 1', [columns, tables], (err, resQuery) => {
    console.log(query.sql);
    console.log("Fetched: ", resQuery.length);
    // Case Error
    if (err) {
      console.log("Error: ", err);
      res.status(500).send(err);
    }
    // Case Ok
    console.log("-- Fin de la requete get Questionnaire");
    res.status(200).send(resQuery);
  });
});

router.get('/insertReponse', (req, res) => {
  res.set('Access-Control-Allow-Origin', '*');
  // Tables Classe ReponseS
  var RStables = ['Reponses'];
  var RScolumns = ['dateReponses', 'sportifReponses', 'questionnaireReponses'];

  // Tables Classe Reponse
  var Rtables = ['Reponse'];
  var Rcolumns = ['reponse', 'lesReponses', 'laQuestion'];

  // Les Arguments
  const { idSportif, idQuestionnaire, reps } = req.query;
  // var formated = d.toISOString().slice(0, 19).replace('T', ' ');
  // console.log(formated);
  /* Converti la date en année et numero de semaine */
  var d = new Date();
  var annee = d.getFullYear();
  // Make Sunday's day number 7
  d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
  // Get first day of year
  var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
  // Calculate full weeks to nearest Thursday
  var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
  console.log("annee :",annee);
  console.log("weekNo Aujourd'hui :", weekNo);

  var query = sql_pool.query('SELECT * FROM Reponses', (err, resQuery) => {
    if(err){
      console.log("Error: ", err);
      res.status(500).send();
    }
    console.log(resQuery);
    let pass = 0;
    var anneeReponses;
    var yearStartReponses;
    var weekNoReponses;
    for(let i = 0; i < resQuery.length && pass === 0; i++){
      anneeReponses = resQuery[i].dateReponses.getFullYear();
      resQuery[i].dateReponses.setUTCDate(resQuery[i].dateReponses.getUTCDate() + 4 - (resQuery[i].dateReponses.getUTCDay()||7));
      yearStartReponses = new Date(Date.UTC(resQuery[i].dateReponses.getUTCFullYear(),0,1));
      weekNoReponses = Math.ceil(( ( (resQuery[i].dateReponses - yearStartReponses) / 86400000) + 1)/7);
      // console.log("annee Reponses :",anneeReponses);
      // console.log("weekNo Reponses :",weekNoReponses);
      // console.log("Sportif de la bdd :",resQuery[i].sportifReponses," -- Sportif actuel : ",idSportif);
      // console.log("Questionnaire de la bdd :",resQuery[i].questionnaireReponses," -- Sportif actuel : ",idQuestionnaire);
      // console.log("Condition annee :",annee === anneeReponses);
      // console.log("Condition weekno :",weekNo-1 === weekNoReponses);
      // console.log("Condition sportif :",parseInt(idSportif) === resQuery[i].sportifReponses);
      // console.log("Condition questionnaire :",parseInt(idQuestionnaire) === resQuery[i].questionnaireReponses);
      if(annee === anneeReponses && weekNo-1 === weekNoReponses && parseInt(idSportif) === resQuery[i].sportifReponses && parseInt(idQuestionnaire) === resQuery[i].questionnaireReponses){
        pass = 1;
        for(let i = 0; i < reps.length; i++){
          // la ça update truc
          console.log("-- i : ",i," -- length : ",reps.length,"\n-- reponse : ",parseInt(reps[i].reponse)," -- idReponses : ",resQuery[i].idReponses," -- idQuestion : ",parseInt(reps[i].idQuestion));
          var query = sql_pool.query('UPDATE Reponse SET reponse = ? WHERE lesReponses = ? AND laQuestion = ?', [parseInt(reps[i].reponse),resQuery[i].idReponses,  parseInt(reps[i].idQuestion)]);
          console.log("querry : ",query.sql);
        }
        console.log("-- Fin de la requete update");
        res.status(200).send();
      }
    }
    if(pass === 0){
      var query = sql_pool.query('INSERT INTO ?? (??) VALUES (NOW(), ?, ?)', [RStables, RScolumns, idSportif, idQuestionnaire]);
      console.log(query.sql);
      var getId = sql_pool.query('SELECT idReponses FROM Reponses WHERE idReponses = (SELECT max(idReponses) FROM Reponses)',(err, resQuery2) => {
        console.log(getId.sql);
        console.log(resQuery2);
        // Case Error
        // Case Ok
        console.log(reps);
        for(let i = 0; i < reps.length; i++){
          console.log("-- i : ",i," -- length : ",reps.length,"\n-- reponse : ",parseInt(reps[i].reponse)," -- idReponses : ",resQuery2," -- idQuestion : ",parseInt(reps[i].idQuestion));
          var query = sql_pool.query('INSERT INTO Reponse (reponse, lesReponses, laQuestion) VALUES (?,?,?)', [parseInt(reps[i].reponse), resQuery2, parseInt(reps[i].idQuestion)]);
        }
        console.log(query.sql);
        console.log("-- Fin de la requete insert");
        res.status(200).send();
      });
    }
  });
  
});

router.get('/newSport', (req, res) => {
  res.set('Access-Control-Allow-Origin', '*');
  const { ns, ps } = req.query;
  var tables = ['Sportif'];
  var query = sql_pool.query('UPDATE ?? SET activiteSportive = ? WHERE pseudoSportif = ?', [tables, ns, ps], (err, resQuery) => {
    console.log(query.sql);
    console.log("Updated: ", query.changes);
    // Case Error
    if (err) {
      console.log("Error: ", err);
      res.status(500).send();
    }
    // Case Ok
    console.log("-- Fin de la new Sport");
    res.status(200).send();
  });
});


router.get('/', (req, res) => {
  res.status(200).send('<marquee behavior="alternate" direction="left" scrollamount="3" scrolldelay="6" truespeed><h1>OK ༼ つ ◕_◕ ༽つ</h1></marquee>');
});


app.use('/', router);
app.use(cors());
app.listen(PORT);
console.log('API is listening at port ' + (PORT));
