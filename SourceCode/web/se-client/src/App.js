import React from 'react';
import FormSportif from './components/FormSportif';
import Login from './components/Login';
import Questionnaire from './components/Questionnaire'
import $ from 'jquery'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      connected: false,
      dataSportif: null,
      questionnaires: null,
      selectedQuestionnaire: null,
      questions: null
    }
  }

  handleChangeQuestionnaires = (data) => {
    this.setState({ questionnaires: data, selectedQuestionnaire : data[0].idQuestionnaire },function(){
      this.getQuestionnaire();
    });
  }

  handleSelectedQuestionnaire = (data) => {
    this.setState({ selectedQuestionnaire : data.target.value },function(){
      this.getQuestionnaire();
    });
  }

  handleLogin = (data) => {
    this.setState({ connected: true, dataSportif: data });
  }

  logMeOut = (data) => {
    this.setState({
      connected: false,
      dataSportif: null,
      questionnaires: null,
      selectedQuestionnaire: 0,
      questions: null
    });
  }

  getQuestionnaire = (event) => {
    let adr = "https://se-l3-ii-back.herokuapp.com";
    $.getJSON(adr + "/getQuestionnaire",{
        idQuestionnaire: this.state.selectedQuestionnaire
    })
    .done((data) => {
        console.log(data);
        this.setState({ questions: data});
    })
  }


  render() {
    return (
      <div className="App container">
        <div className="bg-image"></div>
        <p className="display-1 font-weight-bolder text-center p-4 Title">
          <img src="/logo.png" class="rounded w-25 h-25" alt="Logo"/>
          SportEnough
        </p>
        {!this.state.connected && <Login onLoginOK={this.handleLogin} getQuests={this.handleChangeQuestionnaires}/>}
        {this.state.connected &&
          <div className="Applic row shadow p-4">
            <input className="btn btn-danger btn-lg fixed-top m-3" type="button" value="Déconnexion" onClick={this.logMeOut} />
            <div className="col-sm-4 border border-top-0 border-bottom-0 border-left-0 overflow-auto h-100">
              <FormSportif sportif={this.state.dataSportif} questionnaires={this.state.questionnaires} changeSelected={this.handleSelectedQuestionnaire}/>
            </div>
            <div className="col-sm-8 overflow-auto h-100">
              <Questionnaire questions={this.state.questions} idSportif={this.state.dataSportif.idSportif} idQuestionnaire={this.state.selectedQuestionnaire}/>
            </div>
          </div>
        }
      </div>
    );
  }
}

export default App;
