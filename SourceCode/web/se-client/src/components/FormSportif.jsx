import React, { Component } from 'react'
import $ from 'jquery'

export default class FormSportif extends Component {
    changeSport = (data) => {
        let adr = "https://se-l3-ii-back.herokuapp.com";
        $.getJSON(adr + "/newSport",{
            ns: data.target.value,
            ps: this.props.sportif.pseudoSportif
        })
        .done((data) => {
            console.log(data);
            this.setState({ questions: data});
        })
    }

    render() {
        let listSport =[
            "SKI",
            "BADMINTON",
            "SKATEBOARD",
            "NATATION"
        ]

        const current = new Date();
        const date = `${current.getDate()}/${current.getMonth() + 1}/${current.getFullYear()}`;
        // Make Sunday's day number 7
        current.setUTCDate(current.getUTCDate() + 4 - (current.getUTCDay()||7));
        // Get first day of year
        var yearStart = new Date(Date.UTC(current.getUTCFullYear(),0,1));
        // Calculate full weeks to nearest Thursday
        var weekNo = Math.ceil(( ( (current - yearStart) / 86400000) + 1)/7);
        const birthSportif = new Date(this.props.sportif.dateDeNaissanceSportif);
        const birthSportifFormated = `${birthSportif.getDate()}/${birthSportif.getMonth() + 1}/${birthSportif.getFullYear()}`
        return (
            <div>
                <div className="form-group">
                    <h2>Questionnaire :</h2>
                    <select className="custom-select" onChange={this.props.changeSelected}>
                        {this.props.questionnaires.map(item => (
                            <option key={item.idQuestionnaire} value={item.idQuestionnaire}>{item.titreQuestionnaire}</option>
                        ))}
                    </select>
                </div>
                <h3 className="text-warning">{date}</h3>
                <h3 className="pb-5 text-warning border border-top-0 border-right-0 border-left-0">Semaine : {weekNo}</h3>
                <h2>Profil :</h2>
                <table className="table text-left text-white mt-5">
                    <tbody>
                        <tr key="0">
                            <td className="border-0 font-weight-bold">Nom :</td>
                            <td className="border-0">{this.props.sportif.nomSportif}</td>
                        </tr>
                        <tr key="1">
                            <td className="border-0 font-weight-bold">Prénom :</td>
                            <td className="border-0">{this.props.sportif.prenomSportif}</td>
                        </tr>
                        <tr key="2">
                            <td className="border-0 font-weight-bold">Pseudo :</td>
                            <td className="border-0">{this.props.sportif.pseudoSportif}</td>
                        </tr>
                        <tr key="3">
                            <td className="border-0 font-weight-bold">Date de naissance :</td>
                            <td className="border-0">{birthSportifFormated}</td>
                        </tr>
                        <tr key="4">
                            <td className="border-0 font-weight-bold">Activité Sportive :</td>
                            <td className="border-0 input-group-sm">
                                <select className="custom-select" id="inputGroupSelect" onChange={this.changeSport} defaultValue={this.props.sportif.activiteSportive}>
                                    {listSport.map(item => (
                                        <option key={item} value={item}>{item}</option>
                                    ))}
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}
