import React, { Component } from 'react'
import $ from 'jquery'


export default class ListeSportifs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: null
        }
    }

    sendReponses = (event) => {
        let me = this;
        let listRadioReponse = [];
        let i = 0;
        $(':radio:checked').each(function () {
            listRadioReponse.push({
                idQuestion: me.props.questions[i].idQuestion,
                reponse: $(this).val()
            })
            i++;
        });
        let adr = "https://se-l3-ii-back.herokuapp.com";
        console.log(me.props.idSportif, me.props.idQuestionnaire);
        $.getJSON(adr + "/insertReponse", {
            idSportif: me.props.idSportif,
            idQuestionnaire: me.props.idQuestionnaire,
            reps: listRadioReponse
        })
            .done((msg) => {
                me.setState({status: "OK"});
            })
            .fail(function (jqXHR, textStatus) {
                me.setState({status: "ERROR"});
            });
    }
    render() {
        let i = 0;
        return (
            <div className="ListeSportifs">
                {this.state.status === "OK" ?
                    <div className="container h5">
                        <p className="bg-success text-white border border-primary p-2 font-italic">Ok ! Les réponses ont été envoyées !</p>
                    </div>
                : false}
                {this.state.status === "ERROR" ?
                    <div className="container h5">
                        <p className="bg-danger text-white border border-warning p-2 font-italic">ERREUR Lors de l'envoie du formulaire</p>
                    </div>
                : false}
                {this.props.questions != null ? this.props.questions.map(item => (
                    <div className="border border-white rounded mb-2 shadow " key={i++}>
                        <p className="text-left p-2 pb-3 h3 text-warning" key={i + "-p"}>{i + 1}. <u>{item.textQuestion}</u></p>
                        <form key={i + "-form"}>
                            <div className="d-flex h5" key={i + "-div"} id={"groupe" + i}>
                                <div className="flex-fill">
                                    <input className="mr-1" key={i + "-radio1"} type="radio" id={"rad1-" + i} value="1" name={"groupe" + i} defaultChecked={item.reponseDefaut === 1} ></input>
                                    <label key={i + "-label1"} htmlFor={"rad1-" + i}>Oui</label>
                                </div>
                                <div className="flex-fill">
                                    <input className="mr-1" key={i + "-radio2"} type="radio" id={"rad2-" + i} value="0" name={"groupe" + i} defaultChecked={item.reponseDefaut === 0} ></input>
                                    <label key={i + "-label2"} htmlFor={"rad2-" + i}>Non</label>
                                </div>
                            </div>
                        </form>
                    </div>
                )) : false}
                <input className="btn btn-primary btn-lg float-right" type="button" value="Envoyer" onClick={this.sendReponses} />
            </div>
        )
    }
}
