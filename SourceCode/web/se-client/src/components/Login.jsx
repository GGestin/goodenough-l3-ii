import React, { Component } from 'react'
import $ from 'jquery'
import md5 from 'md5';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false
    }
  }

  logMeIn = (event) => {
    let me = this;
    let id = $("#idInput")[0].value;
    var ps = md5($("#psInput")[0].value);
    if (id && ps) {
      let adr = "https://se-l3-ii-back.herokuapp.com";
      $.ajax({
        url: adr + "/login",
        data: { id: id, ps: ps }
      })
        .done((msg) => {
          $.getJSON(adr + "/get", { id: id })
            .done((msg) => {
              $.getJSON(adr + "/getQuestionnaires")
                .done((data) => {
                  me.props.getQuests(data);
                  console.log(msg[0]);
                  me.props.onLoginOK(msg[0]);
                })
            })
            .fail(function (jqXHR, textStatus) {
              me.setState({error:textStatus});
            });
        })
        .fail(function (jqXHR, textStatus) {
          me.setState({error:"ERREUR Mot de passe / Pseudo."});
        });
    } else {
      me.setState({error:"ERREUR Merci de remplir tout les champs."});
    }
  }

  render() {
    return (
      <div className="Login mx-auto container shadow p-4">
        <h1 className="h1">Connectez-vous</h1>
        <hr />
        {this.state.error &&
          <div className="container h5">
            <p className="bg-danger text-white border border-warning p-2 font-italic">{this.state.error}</p>
          </div>
        }
        <div className="row Form">
          <div className="input-group mb-4">
            <div className="input-group-prepend">
              <span className="input-group-text">Identifiant</span>
            </div>
            <input id="idInput" type="text" className="form-control" placeholder="pseudo..." required />
          </div>
          <div className="input-group mb-4">
            <div className="input-group-prepend">
              <span className="input-group-text">Mot de Passe</span>
            </div>
            <input id="psInput" type="password" className="form-control" placeholder="Mot de passe..." required />
          </div>
        </div>
        <input className="btn btn-primary btn-lg" type="button" value="Connexion" onClick={this.logMeIn} />
      </div>

    )
  }
}
